
// Accept mime types
export const ALL = '*/*';

// Images
export const IMAGE_JPEG = 'image/jpeg,image/pjpeg';
export const IMAGE_PNG = 'image/png';
export const IMAGE_GIF = 'image/gif';
export const IMAGES = [IMAGE_JPEG, IMAGE_PNG, IMAGE_GIF];
