export function getNoPhoto(width, height) {
  return `/static/no-photo/${width}x${height || width}.gif`;
}
