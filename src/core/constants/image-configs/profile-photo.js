export const SIZES = {
  small: { width: 100, height: 100 },
};

export const CROPPER_CONFIG = {
  aspectRatio: 1,
  viewMode: 1,
};
