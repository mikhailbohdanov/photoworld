export default {
  viewMode: 1,
  dragMode: 'none',
  guides: false,
  center: false,
  movable: false,
  rotable: false,
  zoomable: false,
  toggleDragModeOnDblclick: false,
};
