

export class Error {
  constructor(message) {
    this._message = message;
  }

  getKey() {
    return null;
  }

  getMessage() {
    return this._message;
  }
}
