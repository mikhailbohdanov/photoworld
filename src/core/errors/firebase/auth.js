import { InputError } from 'core/errors/input-error';

import authErrorsTranslations from 'views/translations/errors/auth';


export default {
  'user-not-found': [InputError, 'email', authErrorsTranslations.userNotFound],
  'email-already-in-use': [InputError, 'email', authErrorsTranslations.emailAlreadyInUse],

  'wrong-password': [InputError, 'password', authErrorsTranslations.passwordInvalid],

};
