import firebaseErrors from './firebase';


export function fetchErrors(errors) {
  return _.reduce(errors, (prev, error) => ({
    ...prev,
    [error.getKey()]: error.getMessage(),
  }), {});
}

export function fetchFirebaseError(_origin) {
  let ERROR_DATA;

  if (!_origin.code) {
    const [errorPath, errorCode] = _origin.code.split('/');
    ERROR_DATA = _.get(firebaseErrors, [errorPath, errorCode]);
  }

  if (!ERROR_DATA) {
    return {
      _origin,
    };
  }

  const [ErrorClass, ...args] = ERROR_DATA;

  return {
    _origin,
    ...fetchErrors([new ErrorClass(...args)]),
  };
}
