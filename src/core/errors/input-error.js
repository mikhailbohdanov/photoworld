import { Error } from './error';


export class InputError extends Error {
  constructor(input, message) {
    super(message);

    this._input = input;
  }

  getKey() {
    return this._input;
  }
}
