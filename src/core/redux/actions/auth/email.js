import { firebaseAuth } from 'core/firebase';

import createAction from 'core/utils/create-async-action';
import { isEmail } from 'core/utils/validators';

import { fetchErrors, fetchFirebaseError } from 'core/errors';
import { InputError } from 'core/errors/input-error';

import authErrorsTranslations from 'views/translations/errors/auth';


export const AUTH_SIGNIN_EMAIL = createAction('FIREBASE.AUTH.SIGNIN.EMAIL');
const {
  START: SIGNIN_START,
  SUCCESS: SIGNIN_SUCCESS,
  FAIL: SIGNIN_FAIL,
} = AUTH_SIGNIN_EMAIL;

export const AUTH_SIGNUP_EMAIL = createAction('FIREBASE.AUTH.SIGNUP.EMAIL');
const {
  START: SIGNUP_START,
  SUCCESS: SIGNUP_SUCCESS,
  FAIL: SIGNUP_FAIL,
} = AUTH_SIGNUP_EMAIL;


export function signInEmail(email, password) {
  const params = { email, password };

  return function (dispatch) {
    dispatch(SIGNIN_START(params));

    const errors = [];

    if (!email) {
      errors.push(new InputError('email', authErrorsTranslations.emailEmpty));
    } else if (!isEmail(email)) {
      errors.push(new InputError('email', authErrorsTranslations.emailInvalid));
    }

    if (!password) {
      errors.push(new InputError('password', authErrorsTranslations.passwordEmpty));
    }

    return new Promise((resolve, reject) => {
      if (!errors.length) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
          .then(resolve, error => {
            reject(fetchFirebaseError(error));
          });
      } else {
        reject(fetchErrors(errors));
      }
    })
      .then(result => {
        dispatch(SIGNIN_SUCCESS(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(SIGNIN_FAIL(params, errors));
        throw errors;
      });
  };
}

export function signUpEmail(email, password, retryPassword) {
  const params = { email, password, retryPassword };

  return function (dispatch) {
    dispatch(SIGNUP_START(params));

    const errors = [];

    if (!email) {
      errors.push(new InputError('email', authErrorsTranslations.emailEmpty));
    } else if (!isEmail(email)) {
      errors.push(new InputError('email', authErrorsTranslations.emailInvalid));
    }

    if (!password) {
      errors.push(new InputError('password', authErrorsTranslations.passwordEmpty));
    }

    if (password !== retryPassword) {
      errors.push(new InputError('retryPassword', authErrorsTranslations.passwordEquals));
    }

    return new Promise((resolve, reject) => {
      if (!errors.length) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
          .then(resolve, error => {
            reject(fetchFirebaseError(error));
          });
      } else {
        reject(fetchErrors(errors));
      }
    })
      .then(result => {
        dispatch(SIGNUP_SUCCESS(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(SIGNUP_FAIL(params, errors));
        throw errors;
      });
  };
}
