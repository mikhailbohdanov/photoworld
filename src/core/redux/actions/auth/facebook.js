import firebase from 'firebase';

import { firebaseAuth } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { fetchFirebaseError } from 'core/errors';


export const AUTH_CONNECT_FACEBOOK = createAction('FIREBASE.AUTH.CONNECT.FACEBOOK');
const { START, SUCCESS, FAIL } = AUTH_CONNECT_FACEBOOK;


export function connectFacebook() {
  return function (dispatch) {
    dispatch(START(null));

    return firebaseAuth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(result => {
        dispatch(SUCCESS(null, result));
        return result;
      })
      .catch(error => {
        error = fetchFirebaseError(error);

        dispatch(FAIL(null, error));
        throw error;
      });
  };
}
