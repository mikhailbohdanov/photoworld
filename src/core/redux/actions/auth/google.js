import firebase from 'firebase';

import { firebaseAuth } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { fetchFirebaseError } from 'core/errors';


export const AUTH_CONNECT_GOOGLE = createAction('FIREBASE.AUTH.CONNECT.GOOGLE');
const { START, SUCCESS, FAIL } = AUTH_CONNECT_GOOGLE;


export function connectGoogle() {
  return function (dispatch) {
    dispatch(START(null));

    return firebaseAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(result => {
        dispatch(SUCCESS(null, result));
        return result;
      })
      .catch(error => {
        error = fetchFirebaseError(error);

        dispatch(FAIL(null, error));
        throw error;
      });
  };
}
