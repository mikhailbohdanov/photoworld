import { firebaseAuth } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { fetchFirebaseError } from 'core/errors';


export const AUTH_INIT = createAction('FIREBASE.AUTH.INIT');
const { START, SUCCESS, FAIL } = AUTH_INIT;


export default function (dispatch) {
  return new Promise((resolve, reject) => {
    dispatch(START(null));

    const unsubscribe = firebaseAuth.onAuthStateChanged(
      user => {
        dispatch(SUCCESS(null, user));
        unsubscribe();
        resolve();
      },
      error => {
        error = fetchFirebaseError(error);

        dispatch(FAIL(null, error));
        reject(error);
      }
    );
  });
}
