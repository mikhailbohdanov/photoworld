import { firebaseAuth } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { fetchFirebaseError } from 'core/errors';


export const AUTH_SIGNOUT = createAction('FIREBASE.AUTH.SIGNOUT');
const { START, SUCCESS, FAIL } = AUTH_SIGNOUT;


export function signOut() {
  return function (dispatch) {
    dispatch(START());

    return new Promise((resolve, reject) => {
      firebaseAuth.signOut()
        .then(resolve, error => {
          reject(fetchFirebaseError(error));
        });
    })
      .then(() => {
        dispatch(SUCCESS());
        return Promise.resolve(null);
      })
      .catch(error => {
        dispatch(FAIL(null, error));
        throw error;
      });
  };
}
