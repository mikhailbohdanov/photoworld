import { firebaseStorage } from 'core/firebase';

import createAction, { DEFAULT_NAMES } from 'core/utils/create-async-action';


export const FILES_UPLOAD_ACTIONS = createAction('FIREBASE.FS.FILES.UPLOAD', [
  ...DEFAULT_NAMES,
  'PROGRESS',
]);
const { START, PROGRESS, SUCCESS, FAIL } = FILES_UPLOAD_ACTIONS;


export function uploadFiles(files) {
  return dispatch => {

  };
}
