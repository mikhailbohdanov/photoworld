import createAction from 'core/utils/create-async-action';
import { DataStream } from 'core/utils/data-stream.js';

import { Dialog } from 'core/models/im/index';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';


export const DIALOGS_STREAM_ACTIONS = createAction('FIREBASE.DB.DIALOGS.STREAM', [
  'SUBSCRIBE', 'UNSUBSCRIBE',
  'ADD', 'CHANGE', 'REMOVE',
]);
const {
  SUBSCRIBE, UNSUBSCRIBE,
  ADD, CHANGE, REMOVE,
} = DIALOGS_STREAM_ACTIONS;

export const DIALOGS_STREAM = new DataStream('userDialogs', {
  onSubscribe: SUBSCRIBE,
  onUnsubscribe: UNSUBSCRIBE,
  onAdd: ADD,
  onChange: CHANGE,
  onRemove: REMOVE,
}, Dialog);


export function subscribe() {
  return (dispatch, getState) => {
    const myUserId = selectCurrentProfile(getState(), 'uid');
    DIALOGS_STREAM.subscribe(dispatch, myUserId);
  };
}

export function unsubscribe() {
  return dispatch => {
    DIALOGS_STREAM.unsubscribe(dispatch);
  };
}


export * from './update';
export * from './remove';
