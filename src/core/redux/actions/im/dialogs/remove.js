import createAction from 'core/utils/create-async-action';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import { DIALOGS_STREAM } from './index';
import { MESSAGES_STREAM } from 'core/redux/actions/im/messages';


export const DIALOG_REMOVE_ACTIONS = createAction('FIREBASE.DB.DIALOGS.REMOVE');
const { START, SUCCESS, FAIL } = DIALOG_REMOVE_ACTIONS;


export function remove(userId) {
  return (dispatch, getState) => {
    const myUserId = selectCurrentProfile(getState(), 'uid');

    const params = {
      myUserId,
      userId,
    };

    dispatch(START(params));

    return DIALOGS_STREAM
      .remove(myUserId, userId)
      .then(() => {
        return MESSAGES_STREAM
          .remove(myUserId, userId);
      })
      .then(() => {
        dispatch(SUCCESS(params));
        return Promise.resolve();
      })
      .catch(errors => {
        dispatch(FAIL(params, errors));
        throw errors;
      });
  };
}
