import createAction from 'core/utils/create-async-action';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import { DIALOGS_STREAM } from './index';


export const DIALOG_UPDATE_ACTIONS = createAction('FIREBASE.DB.DIALOGS.UPDATE');
const { START, SUCCESS, FAIL } = DIALOG_UPDATE_ACTIONS;


export function update(userId, text, time) {
  return (dispatch, getState) => {
    const myUserId = selectCurrentProfile(getState(), 'uid');

    const dialogData = {
      lastMessageTime: time,
      lastMessageText: text,
    };
    const params = {
      myUserId,
      userId,
      dialogData,
    };

    dispatch(START(params));

    return DIALOGS_STREAM
      .update(dialogData, myUserId, userId)
      .then(() => {
        return DIALOGS_STREAM
          .update(dialogData, userId, myUserId);
      })
      .then(() => {
        dispatch(SUCCESS(params));
        return Promise.resolve();
      })
      .catch(errors => {
        dispatch(FAIL(params, errors));
        throw errors;
      });
  };
}
