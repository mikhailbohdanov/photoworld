import createAction from 'core/utils/create-async-action';
import { DataStream } from 'core/utils/data-stream.js';

import { Message } from 'core/models/im/index';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';


export const MESSAGES_STREAM_ACTIONS = createAction('FIREBASE.DB.MESSAGES.STREAM', [
  'SUBSCRIBE', 'UNSUBSCRIBE',
  'ADD', 'CHANGE', 'REMOVE',
]);
const {
  SUBSCRIBE, UNSUBSCRIBE,
  ADD, CHANGE, REMOVE,
} = MESSAGES_STREAM_ACTIONS;

export const MESSAGES_STREAM = new DataStream('userDialogsMessages', {
  onSubscribe: SUBSCRIBE,
  onUnsubscribe: UNSUBSCRIBE,
  onAdd: ADD,
  onChange: CHANGE,
  onRemove: REMOVE,
}, Message);


export function subscribe(userId) {
  return (dispatch, getState) => {
    const myUserId = selectCurrentProfile(getState(), 'uid');
    MESSAGES_STREAM.subscribe(dispatch, myUserId, userId);
  };
}

export function unsubscribe() {
  return dispatch => {
    MESSAGES_STREAM.unsubscribe(dispatch);
  };
}

export * from './send';
