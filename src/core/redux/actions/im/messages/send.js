import createAction from 'core/utils/create-async-action';

import { MESSAGE_DIRECTION } from 'core/constants/im';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import { MESSAGES_STREAM } from './index';
import { update } from 'core/redux/actions/im/dialogs';


export const MESSAGE_SEND = createAction('FIREBASE.DB.MESSAGES.SEND');
const { START, SUCCESS, FAIL } = MESSAGE_SEND;


export function sendMessage(toUserId, text) {
  return function (dispatch, getState) {
    const myUserId = selectCurrentProfile(getState(), 'uid');

    const time = new Date().getTime();
    const message = {
      text,
      time,
      read: false,
    };

    const params = {
      myUserId,
      toUserId,
      text,
      time,
    };

    dispatch(START(params));

    return MESSAGES_STREAM
      .push({
        ...message,
        direction: MESSAGE_DIRECTION.OUT,
        read: false,
      }, myUserId, toUserId)
      .then(myMessage => {
        return MESSAGES_STREAM
          .push({
            ...message,
            direction: MESSAGE_DIRECTION.IN,
            read: false,
            originId: myMessage._id,
          }, toUserId, myUserId);
      })
      .then(() => {
        dispatch(update(toUserId, text, time));
      })
      .then(result => {
        dispatch(SUCCESS(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(FAIL(params, errors));
        throw errors;
      });
  };
}
