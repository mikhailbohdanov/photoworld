import firebase from 'firebase';
import md5 from 'md5';

import { firebaseStorage } from 'core/firebase';

import { fetchFirebaseError } from 'core/errors';

import createAction, { DEFAULT_NAMES } from 'core/utils/create-async-action';


export const PHOTO_UPLOAD_ACTIONS = createAction('FIREBASE.FS.PROFILES.PHOTO.UPLOAD', [
  ...DEFAULT_NAMES,
  'PROGRESS',
]);
const { START, PROGRESS, SUCCESS, FAIL } = PHOTO_UPLOAD_ACTIONS;


export function uploadPhotos(path, files) {
  return dispatch => {
    const filesPromises = _.map(files, (fileData, index) => {
      const fileName = md5(new Date().getTime());
      const params = {
        path,
        files,
        fileName,
        index,
        fullName: `${path}/${fileName}`,
      };

      return new Promise((resolve, reject) => {
        dispatch(START(params));

        const ref = firebaseStorage
          .ref(params.fullName)
          .put(fileData);

        ref.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          snapshot => dispatch(PROGRESS(params, snapshot)),
          error => reject(fetchFirebaseError(error)),
          () => resolve(params),
        );
      });
    });

    return Promise.all(filesPromises)
      .then(result => {
        dispatch(SUCCESS(null, result));
        return result;
      })
      .catch(errors => {
        dispatch(FAIL(null, errors));
        throw errors;
      });
  };
}
