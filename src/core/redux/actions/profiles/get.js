import { firebaseDb } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { fetchFirebaseError } from 'core/errors';

import {
  selectProfileInfo,
} from 'core/redux/selectors/profiles/profile';


export const PROFILE_INFO_GET = createAction('FIREBASE.DB.PROFILES.INFO.GET');
const {
  START: START_INFO,
  SUCCESS: SUCCESS_INFO,
  FAIL: FAIL_INFO,
} = PROFILE_INFO_GET;

export const PROFILE_DETAILS_GET = createAction('FIREBASE.DB.PROFILES.DETAILS.GET');
const {
  START: START_DETAILS,
  SUCCESS: SUCCESS_DETAILS,
  FAIL: FAIL_DETAILS,
} = PROFILE_DETAILS_GET;


export function getProfileInfo(userId, force = false) {
  return function (dispatch, getState) {
    const profileInfo = selectProfileInfo(getState(), userId);

    if (profileInfo && !force) {
      return Promise.resolve(profileInfo);
    }

    const params = {
      userId,
    };

    dispatch(START_INFO(params));

    return new Promise((resolve, reject) => {
      firebaseDb
        .ref(`userProfiles/${userId}`)
        .once('value')
        .then(data => {
          const value = data.val();

          if (value) {
            resolve(value);
          } else {
            // TODO change it to real error!!!
            reject(null);
          }
        }, error => {
          reject(fetchFirebaseError(error));
        });
    })
      .then(result => {
        dispatch(SUCCESS_INFO(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(FAIL_INFO(params, errors));
        throw errors;
      });
  };
}

export function getProfileDetails(userId) {
  return function (dispatch) {
    const params = {
      userId,
    };

    dispatch(START_DETAILS(params));

    return new Promise((resolve, reject) => {
      firebaseDb
        .ref(`userProfilesDetails/${userId}`)
        .once('value')
        .then(data => {
          const value = data.val();

          if (value) {
            resolve(value);
          } else {
            // TODO change it to real error!!!
            reject(null);
          }
        }, error => {
          throw fetchFirebaseError(error);
        });
    })
      .then(result => {
        dispatch(SUCCESS_DETAILS(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(FAIL_DETAILS(params, errors));
        throw errors;
      });
  };
}
