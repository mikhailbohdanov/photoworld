import { firebaseDb } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { FRIEND_TYPE } from 'core/constants/lists/friends';

import { fetchFirebaseError } from 'core/errors';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import { PROFILE_INFO_GET } from 'core/redux/actions/profiles/get';

const FRIEND_LIST_GET = createAction('FIREBASE.DB.FRIENDS.LIST.GET');
const {
  START: LIST_GET_START,
  SUCCESS: LIST_GET_SUCCESS,
  FAIL: LIST_GET_FAIL,
} = FRIEND_LIST_GET;

const FRIEND_ADD = createAction('FIREBASE.DB.FRIENDS.ADD');
const {
  START: ADD_START,
  SUCCESS: ADD_SUCCESS,
  FAIL: ADD_FAIL,
} = FRIEND_ADD;

const FRIEND_CHECK = createAction('FIREBASE.DB.FRIENDS.CHECK');
const {
  START: CHECK_START,
  SUCCESS: CHECK_SUCCESS,
  FAIL: CHECK_FAIL,
} = FRIEND_CHECK;


export function getFriendList(userId) {
  return function (dispatch) {
    const params = {
      userId,
    };

    dispatch(LIST_GET_START(params));

    return new Promise((resolve, reject) => {
      firebaseDb
        .ref('userProfiles')
        .once('value')
        .then(data => {
          const value = data.val();

          _.each(value, (profile, userId) => {
            dispatch(PROFILE_INFO_GET.SUCCESS({ userId }, profile));
          });

          if (value) {
            resolve(value);
          } else {
            reject(null);
          }
        }, error => {
          resolve(fetchFirebaseError(error));
        });
    })
      .then(result => {
        dispatch(LIST_GET_SUCCESS(params, result));
        return result;
      })
      .catch(error => {
        dispatch(LIST_GET_FAIL(params, error));
        throw error;
      });
  };
}

export function addToFriend(userId) {
  return function (dispatch, getState) {
    const myUserId = selectCurrentProfile(getState(), 'uid');
    const params = {
      userId,
      myUserId,
    };

    dispatch(ADD_START(params));

    return new Promise((resolve, reject) => {
      const addToMePromise = firebaseDb
        .ref(`userFriends/${myUserId}/${userId}`)
        .set({
          type: FRIEND_TYPE.MY_REQUEST,
        });

      const addToUserPromise = firebaseDb
        .ref(`userFriends/${userId}/${myUserId}`)
        .set({
          type: FRIEND_TYPE.ME_REQUEST,
        });

      Promise.all([
        addToMePromise,
        addToUserPromise,
      ])
        .then(resolve, reject);
    })
      .then(result => {
        dispatch(ADD_SUCCESS(params, result));
        return result;
      })
      .catch(error => {
        dispatch(ADD_FAIL(params, error));
        throw error;
      });
  };
}

export function checkUserType(userId) {
  return function (dispatch, getState) {
    const myUserId = selectCurrentProfile(getState(), 'uid');
    const params = {
      userId,
      myUserId,
    };

    dispatch(CHECK_START(params));

    return new Promise((resolve, reject) => {
      firebaseDb
        .ref(`userFriends/${myUserId}/${userId}`)
        .once('value')
        .then(data => {
          const value = data.val();

          if (value) {
            resolve(value);
          } else {
            reject(null);
          }
        }, error => {
          reject(fetchFirebaseError(error));
        });
    })
      .then(result => {
        dispatch(CHECK_SUCCESS(params, result));
        return result;
      })
      .catch(error => {
        dispatch(CHECK_FAIL(params, error));
        throw error;
      });
  };
}
