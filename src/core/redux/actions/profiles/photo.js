import { firebaseAuth, firebaseStorage } from 'core/firebase';

import createAction, { DEFAULT_NAMES } from 'core/utils/create-async-action';


export const UPLOAD_PROFILE_PHOTO = createAction('FIREBASE.FS.PROFILES.PHOTO.UPLOAD', [
  ...DEFAULT_NAMES,
  'PROGRESS',
]);
const {
  START: START_UPLOAD,
  SUCCESS: SUCCESS_UPLOAD,
  PROGRESS: PROGRESS_UPLOAD,
  FAIL: FAIL_UPLOAD,
} = UPLOAD_PROFILE_PHOTO;


export function uploadProfilePhoto(photoData) {
  const userId = firebaseAuth.currentUser.uid;

  const params = {
    userId,
    photoData,
  };

  return function (dispatch) {
    dispatch(START_UPLOAD(params));

    return firebaseStorage
      .ref(`${userId}/main-photo.jpg`)
      .put(photoData)
      .then(() => {
        dispatch(SUCCESS_UPLOAD(params));
        return Promise.resolve();
      })
      .catch(() => {
        dispatch(FAIL_UPLOAD(params));
        return Promise.reject();
      });
  };
}
