import createAction from 'core/utils/create-async-action';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';
import {
  updateProfileInfo,
  updateProfileDetails,
} from '../update';


export const FILL_PROFILE_INFO = createAction('FIREBASE.DB.PROFILES.SIGNUP.FILL-PROFILE-INFO');
const { START, SUCCESS, FAIL } = FILL_PROFILE_INFO;

const INFO_FIELDS = ['firstName', 'lastName'];
const DETAILS_FIELDS = ['birthDay', 'city', 'phone', 'email', 'website', 'interests', 'aboutMe'];


export function fillProfileInfo(profileData) {
  return function (dispatch, getState) {
    const userId = selectCurrentProfile(getState(), 'uid');

    if (!userId) {
      return Promise.reject(null);
    }

    const profileInfo = _.pick(profileData, INFO_FIELDS);
    const profileDetails = _.pick(profileData, DETAILS_FIELDS);

    const params = {
      userId,
      profileInfo,
      profileDetails,
    };

    dispatch(START(params));

    const promiseOfInfo = dispatch(updateProfileInfo(profileInfo));
    const promiseOfDetails = dispatch(updateProfileDetails(profileDetails));

    return Promise.all([
      promiseOfInfo,
      promiseOfDetails,
    ])
      .then(() => {
        dispatch(SUCCESS(params, null));
        return null;
      })
      .catch(() => {
        dispatch(FAIL(params, null));
        throw null;
      });
  };
}
