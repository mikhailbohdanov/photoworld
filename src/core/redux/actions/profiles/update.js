import { firebaseDb } from 'core/firebase';

import createAction from 'core/utils/create-async-action';

import { fetchFirebaseError } from 'core/errors';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';


export const PROFILE_INFO_UPDATE = createAction('FIREBASE.DB.PROFILES.INFO.UPDATE');
const {
  START: INFO_START,
  SUCCESS: INFO_SUCCESS,
  FAIL: INFO_FAIL,
} = PROFILE_INFO_UPDATE;

export const PROFILE_DETAILS_UPDATE = createAction('FIREBASE.DB.PROFILES.DETAILS.UPDATE');
const {
  START: DETAILS_START,
  SUCCESS: DETAILS_SUCCESS,
  FAIL: DETAILS_FAIL,
} = PROFILE_DETAILS_UPDATE;


export function updateProfileInfo(profileInfo) {
  return function (dispatch, getState) {
    const userId = selectCurrentProfile(getState(), 'uid');
    const params = {
      userId,
      profileInfo,
    };

    dispatch(INFO_START(params));

    return new Promise(resolve => {
      if (!userId) {
        throw null;
      }

      firebaseDb
        .ref(`userProfiles/${userId}`)
        .update(profileInfo)
        .then(resolve, error => {
          throw fetchFirebaseError(error);
        });
    })
      .then(result => {
        dispatch(INFO_SUCCESS(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(INFO_FAIL(params, errors));
        throw errors;
      });
  };
}

export function updateProfileDetails(profileDetails) {
  return function (dispatch, getState) {
    const userId = selectCurrentProfile(getState(), 'uid');
    const params = {
      userId,
      profileDetails,
    };

    dispatch(DETAILS_START(params));

    return new Promise(resolve => {
      if (!userId) {
        throw null;
      }

      firebaseDb
        .ref(`userProfilesDetails/${userId}`)
        .update(profileDetails)
        .then(resolve, error => {
          throw fetchFirebaseError(error);
        });
    })
      .then(result => {
        dispatch(DETAILS_SUCCESS(params, result));
        return result;
      })
      .catch(errors => {
        dispatch(DETAILS_FAIL(params, errors));
        throw errors;
      });
  };
}
