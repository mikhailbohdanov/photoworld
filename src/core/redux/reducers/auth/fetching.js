import createFetcher from 'core/utils/create-fetcher';

import { AUTH_INIT } from 'core/redux/actions/auth';
import { AUTH_SIGNIN_EMAIL, AUTH_SIGNUP_EMAIL } from 'core/redux/actions/auth/email';
import { AUTH_SIGNOUT } from 'core/redux/actions/auth/signout';


export default createFetcher([
  AUTH_INIT,
  AUTH_SIGNIN_EMAIL,
  AUTH_SIGNUP_EMAIL,
  AUTH_SIGNOUT,
]);
