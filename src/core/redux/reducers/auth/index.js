import { combineReducers } from 'redux';

import user from './user';
import fetching from './fetching';


export default combineReducers({
  user,
  fetching,
});
