import { handleActions } from 'redux-actions';

import { AUTH_INIT } from 'core/redux/actions/auth';
import { AUTH_SIGNIN_EMAIL, AUTH_SIGNUP_EMAIL } from 'core/redux/actions/auth/email';
import { AUTH_CONNECT_FACEBOOK } from 'core/redux/actions/auth/facebook';
import { AUTH_CONNECT_GOOGLE } from 'core/redux/actions/auth/google';
import { AUTH_SIGNOUT } from 'core/redux/actions/auth/signout';


const DEFAULT_STATE = {
  authenticated: false,
  uid: null,
};

const authHandler = (state, { payload }) => ({
  ...state,
  authenticated: _.has(payload, 'response'),
  uid: _.get(payload, 'response.uid', null),
});
const authConnectHandler = (state, { payload }) => ({
  ...state,
  authenticated: _.has(payload, 'response.user'),
  uid: _.get(payload, 'response.user.uid', null),
});
const anonymousHandler = state => ({
  ...state,
  ...DEFAULT_STATE,
});


export default handleActions({
  [AUTH_INIT.SUCCESS]: authHandler,
  [AUTH_SIGNIN_EMAIL.SUCCESS]: authHandler,
  [AUTH_SIGNUP_EMAIL.SUCCESS]: authHandler,
  [AUTH_CONNECT_FACEBOOK.SUCCESS]: authConnectHandler,
  [AUTH_CONNECT_GOOGLE.SUCCESS]: authConnectHandler,

  [AUTH_SIGNOUT.SUCCESS]: anonymousHandler,
}, DEFAULT_STATE);
