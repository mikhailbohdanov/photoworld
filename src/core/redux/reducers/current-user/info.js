import { handleActions } from 'redux-actions';

import {
} from 'core/redux/actions/profiles/get';


const DEFAULT_STATE = {
  complete: false,
};


export default handleActions({
}, DEFAULT_STATE);
