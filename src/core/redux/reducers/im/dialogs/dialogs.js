import { handleActions } from 'redux-actions';

import { DIALOGS_STREAM_ACTIONS } from 'core/redux/actions/im/dialogs';


const DEFAULT_STATE = {};
const getDialog = (state, { payload: { response } }) => ({
  ...state,
  [response._id]: response,
});


export default handleActions({
  [DIALOGS_STREAM_ACTIONS.ADD]: getDialog,
  [DIALOGS_STREAM_ACTIONS.CHANGE]: getDialog,
  [DIALOGS_STREAM_ACTIONS.REMOVE]: (state, { payload: { response } }) => _.pull(state, response._id),

  [DIALOGS_STREAM_ACTIONS.SUBSCRIBE]: () => DEFAULT_STATE,
  [DIALOGS_STREAM_ACTIONS.UNSUBSCRIBE]: () => DEFAULT_STATE,
}, DEFAULT_STATE);
