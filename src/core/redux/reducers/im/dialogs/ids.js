import { handleActions } from 'redux-actions';

import { DIALOGS_STREAM_ACTIONS } from 'core/redux/actions/im/dialogs';


const DEFAULT_STATE = {
  ordered: [],
  list: {},
};
function sortDialogs(list) {
  return {
    list,
    ordered: _(list)
      .keys()
      .sortBy(list, _id => _.get(list, _id, 0))
      .reverse()
      .value()
  };
}


export default handleActions({
  [DIALOGS_STREAM_ACTIONS.ADD]: ({ list }, { payload: { response } }) => sortDialogs({
    ...list,
    [response._id]: response.lastMessageTime,
  }),
  [DIALOGS_STREAM_ACTIONS.CHANGE]: ({ list }, { payload: { response } }) => sortDialogs({
    ...list,
    [response._id]: response.lastMessageTime,
  }),
  [DIALOGS_STREAM_ACTIONS.REMOVE]: ({ list }, { payload: { response } }) => sortDialogs(_.omit(list, response._id)),

  [DIALOGS_STREAM_ACTIONS.SUBSCRIBE]: () => DEFAULT_STATE,
  [DIALOGS_STREAM_ACTIONS.UNSUBSCRIBE]: () => DEFAULT_STATE,
}, DEFAULT_STATE);
