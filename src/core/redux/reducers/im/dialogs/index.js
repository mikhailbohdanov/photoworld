import { combineReducers } from 'redux';

import ids from './ids';
import dialogs from './dialogs';


export default combineReducers({
  ids,
  dialogs,
});
