import { handleActions } from 'redux-actions';

import { MESSAGES_STREAM_ACTIONS } from 'core/redux/actions/im/messages';


const DEFAULT_STATE = [];


export default handleActions({
  [MESSAGES_STREAM_ACTIONS.ADD]: (state, { payload: { response } }) => ([
    ...state,
    response._id,
  ]),

  [MESSAGES_STREAM_ACTIONS.SUBSCRIBE]: () => DEFAULT_STATE,
  [MESSAGES_STREAM_ACTIONS.UNSUBSCRIBE]: () => DEFAULT_STATE,
}, DEFAULT_STATE);
