import { combineReducers } from 'redux';

import ids from './ids';
import messages from './messages';


export default combineReducers({
  ids,
  messages,
});
