import { combineReducers } from 'redux';

import auth from './auth';
import im from './im';
import profiles from './profiles';
import currentUser from './current-user';


export default combineReducers({
  // routing: routerReducer,

  auth,
  im,
  profiles,
  currentUser,
});
