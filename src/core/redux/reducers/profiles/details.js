import { handleActions } from 'redux-actions';

import { PROFILE_DETAILS_GET } from 'core/redux/actions/profiles/get';


const DEFAULT_STATE = {};


export default handleActions({
  [PROFILE_DETAILS_GET.SUCCESS]: (state, action) => action.payload.response,

}, DEFAULT_STATE);
