import { handleActions } from 'redux-actions';

import { PROFILE_INFO_GET } from 'core/redux/actions/profiles/get';


const DEFAULT_STATE = {};


export default handleActions({
  [PROFILE_INFO_GET.SUCCESS]: (state, { payload: { params, response } }) => ({
    ...state,
    [params.userId]: response,
  }),

}, DEFAULT_STATE);
