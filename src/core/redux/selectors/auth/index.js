export function isAuthenticated(state) {
  return _.get(state, 'auth.user.authenticated', false);
}
