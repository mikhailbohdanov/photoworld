export function selectDialogIds(state) {
  return _.get(state, ['im', 'dialogs', 'ids', 'ordered'], []);
}

export function selectDialogs(state) {
  return _.get(state, ['im', 'dialogs', 'dialogs'], {});
}

export function selectDialog(state, userId) {
  return _.get(state, ['im', 'dialogs', 'dialogs', userId]);
}

export function selectDialogList(state) {
  const dialogIds = selectDialogIds(state);
  const dialogs = selectDialogs(state);

  return _.map(dialogIds, dialogId => _.get(dialogs, dialogId));
}
