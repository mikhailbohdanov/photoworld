export function selectMessageIds(state) {
  return _.get(state, ['im', 'messages', 'ids'], []);
}

export function selectMessages(state) {
  return _.get(state, ['im', 'messages', 'messages'], {});
}

export function selectMessageList(state) {
  const messageIds = selectMessageIds(state);
  const messageObjects = selectMessages(state);

  return _.map(messageIds, messageId => messageObjects[messageId]);
}
