
export function isFetching(state, moduleName) {
  return state[moduleName].fetching;
}

export function selectErrors(state, moduleName) {
  return state[moduleName].errors;
}
