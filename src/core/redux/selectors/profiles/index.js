export function selectCurrentProfile(state, ...keys) {
  // return _.get(state, ['profiles', 'currentUser', ...keys], null);
  return _.get(state, ['auth', 'user', ...keys], null);
}
