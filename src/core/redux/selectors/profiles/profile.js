
export function selectProfileInfo(state, userId) {
  return _.get(state, ['profiles', 'list', userId], null);
}

export function selectProfileDetails(state) {
  return _.get(state, ['profiles', 'details'], null);
}

