import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import CSSModules from 'react-css-modules';


const CSS_MODULES_OPTIONS = {
  allowMultiple: true,
  handleNotFoundStyleName: 'log',
};


export default function (mapStateToProps, mapDispatchToProps, styles) {
  return function (WrappedComponent) {
    if (styles) {
      WrappedComponent = CSSModules(styles, CSS_MODULES_OPTIONS)(WrappedComponent);
    }

    if (mapStateToProps || mapDispatchToProps) {
      WrappedComponent = connect(mapStateToProps, mapDispatchToProps)(WrappedComponent);
    }

    return injectIntl(WrappedComponent);
  };
}
