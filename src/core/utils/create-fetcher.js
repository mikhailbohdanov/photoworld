import { handleActions } from 'redux-actions';


const enable = _.constant(true);
const disable = _.constant(false);


export default function (actions, enableActions = ['START'], disableActions = ['SUCCESS', 'FAIL']) {
  const mappedActions = _.reduce(actions, (mappedActions, action) => ({
    ...mappedActions,
    ..._.reduce(enableActions, (prev, actionName) => ({
      ...prev,
      [action[actionName]]: enable,
    }), {}),
    ..._.reduce(disableActions, (prev, actionName) => ({
      ...prev,
      [action[actionName]]: disable,
    }), {}),
  }), {});

  return handleActions(mappedActions, false);
}
