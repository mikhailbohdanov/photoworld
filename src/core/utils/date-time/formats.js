import moment from 'moment';


export function dateTime2Friendly(timestamp) {
  return moment(timestamp)
    .format('MMM D, YYYY, HH:mm');
}
