import { firebaseStorage } from 'core/firebase';

import { fetchFirebaseError } from 'core/errors';


export class FileStream {
  _path = null;
  _actions = null;
  _refs = [];

  constructor(path, actions = {}) {
    this._path = path;
    this._actions = actions;
  }

  push(value, ...addPath) {
    const path = [this._path, ...addPath];

    return new Promise((resolve, reject) => {
      const ref = firebaseDb
        .ref(path.join('/'))
        .push(value, error => error ? reject(fetchFirebaseError(error)) : resolve(this.unwrapSnapshot(ref)));
    });
  }

  set(value, ...addPath) {
    const path = [this._path, ...addPath];

    return new Promise((resolve, reject) => {
      const ref = firebaseDb
        .ref(path.join('/'))
        .set(value, error => error ? reject(fetchFirebaseError(error)) : resolve(this.unwrapSnapshot(ref)));
    });
  }

  update(value, ...addPath) {
    const path = [this._path, ...addPath];

    return new Promise((resolve, reject) => {
      const ref = firebaseDb
        .ref(path.join('/'))
        .update(value, error => error ? reject(fetchFirebaseError(error)) : resolve(this.unwrapSnapshot(ref)));
    });
  }

  remove(...addPath) {
    const path = [this._path, ...addPath];

    return new Promise((resolve, reject) => {
      const ref = firebaseDb
        .ref(path.join('/'))
        .remove(error => error ? reject(fetchFirebaseError(error)) : resolve(this.unwrapSnapshot(ref)));
    });
  }

  subscribe(emitter, ...addPath) {
    if (this._ref) {
      return;
    }

    const {
      onSubscribe,
      onLoad,
      onAdd,
      onChange,
      onMove,
      onRemove,
    } = this._actions;

    const path = [this._path, ...addPath];
    let initialized = false;
    const items = {};

    this._ref = firebaseDb.ref(path.join('/'));

    if (onSubscribe) {
      emitter(onSubscribe(this.getParams()));
    }

    if (onLoad) {
      this._ref.once('value', () => {
        initialized = true;
        emitter(onLoad(
          this.getParams(),
          items,
        ));
      });
    } else {
      initialized = true;
    }

    if (onLoad || onAdd) {
      this._ref.on('child_added', snapshot => {
        if (initialized && onAdd) {
          emitter(onAdd(
            this.getParams(snapshot),
            this.unwrapSnapshot(snapshot),
          ));
        }

        if (!initialized && onLoad) {
          items[snapshot.key] = this.unwrapSnapshot(snapshot);
        }
      });
    }

    if (onChange) {
      this._ref.on('child_changed', snapshot => {
        emitter(onChange(
          this.getParams(snapshot),
          this.unwrapSnapshot(snapshot),
        ));
      });
    }

    if (onMove) {
      this._ref.on('child_moved', snapshot => {
        emitter(onMove(
          this.getParams(snapshot),
          this.unwrapSnapshot(snapshot),
        ));
      });
    }

    if (onRemove) {
      this._ref.on('child_removed', snapshot => {
        emitter(onRemove(
          this.getParams(snapshot),
          this.unwrapSnapshot(snapshot),
        ));
      });
    }
  }

  unsubscribe(emitter) {
    if (this._ref) {
      const { onUnsubscribe } = this._actions;

      this._ref.off();

      if (onUnsubscribe) {
        emitter(onUnsubscribe(this.getParams()));
      }

      this._ref = null;
    }
  }
}
