import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import ru from 'react-intl/locale-data/ru';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Cookie from 'js-cookie';

import configureStore from 'core/redux/store';
import initializeAuth from 'core/redux/actions/auth';

import Root from 'views/root';

import 'views/styles/styles.css';
import 'cropperjs/dist/cropper.css';

import locales from 'views/static/locales/data.json';

addLocaleData([...en, ...ru]);
injectTapEventPlugin();

const store = configureStore();

const rootElement = document.getElementById('root');

const locale = Cookie.get('locale') || 'ru';


function render(RootElement) {
  ReactDOM.render(
    <AppContainer>
      <IntlProvider locale={locale} messages={locales[locale]}>
        <RootElement store={store} />
      </IntlProvider>
    </AppContainer>,
    rootElement
  );
}

if (module.hot) {
  module.hot.accept('views/root', () => {
    render(require('views/root').default);
  });
}

initializeAuth(store.dispatch)
  .then(() => render(Root))
  .catch(error => console.error(error)); // eslint-disable-line no-console
