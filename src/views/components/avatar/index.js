import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import ConnectAll from 'core/utils/connect-all';

import { getNoPhoto } from 'core/constants/image-configs/no-photo';

import Image from 'views/components/image';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class Avatar extends Component {
  static propTypes = {
    className: PropTypes.string,
    imageClassName: PropTypes.string,
    contentClassName: PropTypes.string,
    url: PropTypes.string,
    size: PropTypes.number,
    children: PropTypes.any,
    border: PropTypes.bool,
    onImageClick: PropTypes.func,
  };

  static defaultProps = {
    size: 40,
    border: false,
  };

  get image() {
    let { url, size } = this.props;

    return (
      <div styleName="image"
           onClick={this.props.onImageClick}>
        <Image preloader
               src={url}
               emptySrc={getNoPhoto(size)}
               width={size}
               height={size} />
      </div>
    );
  }

  get hoverContent() {
    const { children } = this.props;

    if (!children) {
      return null;
    }

    return (
      <div styleName="content">
        {children}
      </div>
    );
  }

  render() {
    const { className, border, size } = this.props;

    return (
      <div style={{ width: `${size}px`, height: `${size}px` }}
           className={className}
           styleName={classNames('wrapper', { border })}>
        {this.image}
        {this.hoverContent}
      </div>
    );
  }
}
