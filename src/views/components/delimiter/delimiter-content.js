import React from 'react';
import PropTypes from 'prop-types';

import ConnectAll from 'core/utils/connect-all';

import styles from './index.styl';


export function DelimiterContent({ className, children }) {
  return (
    <div className={className}
         styleName="delimiter-content">
      {children}
    </div>
  );
}

DelimiterContent.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default ConnectAll(null, null, styles)(DelimiterContent);
