import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';

import DelimiterContent from './delimiter-content';

import styles from './index.styl';


export const DELIMITER_PROP_TYPES = {
  className: PropTypes.string,
  delimiterClassName: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]),
  delimiterText: PropTypes.node,
};

export function buildDelimiter(styleName) {

  @CSSModules(styles)
  class Delimiter extends Component {
    static displayName = `delimiter-${styleName}`;
    static propTypes = DELIMITER_PROP_TYPES;

    get separatedChildren() {
      let { children, delimiterText, delimiterClassName } = this.props;
      let childIndex = 0;

      children = React.Children.toArray(children);

      return _.reduce(children, (prev, child, index) => {
        prev.push(React.cloneElement(child, {
          key: childIndex++,
        }));

        if (index < children.length - 1) {
          prev.push(
            <div key={childIndex++}
                 className={delimiterClassName}
                 styleName="delimiter">
              <span styleName="delimiter-text">
                {delimiterText}
              </span>
            </div>
          );
        }

        return prev;
      }, []);
    }

    render() {
      const { className } = this.props;

      return (
        <div className={className}
             styleName={styleName}>
          {this.separatedChildren}
        </div>
      );
    }
  }

  return Delimiter;
}

export const DelimiterHorizontal = buildDelimiter('horizontal');
export const DelimiterVertical = buildDelimiter('vertical');
export { DelimiterContent };
