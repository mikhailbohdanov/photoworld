import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ConnectAll from 'core/utils/connect-all';

import { CircularProgress } from 'material-ui';

import styles from './center.styl';


@ConnectAll(null, null, styles)
export class CenterFetcher extends Component {
  static propTypes = {
    SpinnerComponent: PropTypes.any,
  };

  static defaultProps = {
    SpinnerComponent: CircularProgress
  };

  get spinner() {
    const { SpinnerComponent } = this.props;
    const props = _.omit(this.props, 'SpinnerComponent', 'intl');

    return (
      <SpinnerComponent {...props} />
    );
  }

  render() {
    return (
      <div styleName="wrapper">
        {this.spinner}
      </div>
    )
  }
}
