import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { firebaseStorage } from 'core/firebase';


export default class Image extends Component {
  static propTypes = {
    className: PropTypes.string,
    src: PropTypes.string,
    emptySrc: PropTypes.string,
    alt: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    preloader: PropTypes.bool,
  };

  static defaultProps = {
    preloader: false,
  };

  state = {
    imageUrl: null,
  };

  componentWillMount() {
    this.updateImageUrl();
  }

  componentDidUpdate() {
    this.updateImageUrl();
  }

  updateImageUrl() {
    const { src } = this.props;

    if (src) {
      firebaseStorage
        .ref(src)
        .getDownloadURL()
        .then(url => {
          this.setImageUrl(url);
        })
        .catch(() => {
          this.setEmpty();
        });
    } else {
      this.setEmpty();
    }
  }

  setEmpty() {
    this.setImageUrl(this.props.emptySrc);
  }

  setImageUrl(imageUrl) {
    this.setState({
      imageUrl,
    });
  }

  get preloader() {
    if (!this.props.preloader) {
      return null;
    }

    return (
      <div />
    );
  }

  render() {
    const { imageUrl } = this.state;

    if (!imageUrl) {
      return this.preloader;
    }

    const { className, alt, width, height } = this.props;

    return (
      <img className={className}
           alt={alt}
           width={width}
           height={height}
           src={imageUrl} />
    );
  }
}
