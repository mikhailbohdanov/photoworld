import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';

import ConnectAll from 'core/utils/connect-all';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class Link extends Component {
  render() {
    const props = _.pick(this.props, 'children', 'to', 'className', 'activeClassName');

    return (
      <RouterLink {...props}
                  styleName="link" />
    );
  }
}
