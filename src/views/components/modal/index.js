import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import PubSub from 'pubsub-js';

import { MODAL } from 'core/constants/messages';

import {
  Dialog,
} from 'material-ui';


const DEFAULT_STATE = {
  title: null,
  actionButtons: null,
  content: null,
  open: false,
};


export class Modal extends Component {
  state = DEFAULT_STATE;

  componentDidMount() {
    PubSub.subscribe(MODAL.OPEN, this.open);
    PubSub.subscribe(MODAL.CLOSE, this.close);
  }

  componentWillUnmount() {
    PubSub.unsubscribe(MODAL.OPEN, this.open);
    PubSub.unsubscribe(MODAL.CLOSE, this.close);
  }

  @autobind
  open(message, config) {
    this.setState({
      ...config,
      open: true,
    });
  }

  @autobind
  close() {
    this.setState({
      ...DEFAULT_STATE,
    });
  }

  render() {
    const {
      title,
      actions,
      content,
      open,
    } = this.state;

    return (
      <Dialog title={title}
              actions={actions}
              modal={false}
              open={open}
              onRequestClose={this.close}>
        {content}
      </Dialog>
    );
  }
}

export function openModal(content, options) {
  PubSub.publish(MODAL.OPEN, {
    content,
    ...options,
  });
}

export function closeModal() {
  PubSub.publish(MODAL.CLOSE);
}
