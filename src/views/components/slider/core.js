import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import styles from './core.styl';


@ConnectAll(null, null, styles)
export default class SliderCore extends Component {
  static propTypes = {
    getRef: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.any,
    activeSlide: PropTypes.number,
  };

  static defaultProps = {
    getRef: _.noop,
  };

  state = {
    currentSlideIndex: 0,
    wrapperHeight: 0,
    slidesContainerLeftOffset: 0,
  };

  componentDidMount() {
    this.calcSlideData(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.activeSlide !== nextProps.activeSlide) {
      this.changeActiveSlide(nextProps.activeSlide);
    }
  }

  _wrapper = null;
  _slidesContainer = null;
  _slides = {};

  slidesCount = 0;
  slideWidth = 0;
  slidesContainerWidth = 0;

  @autobind
  saveRefWrapper(element) {
    this._wrapper = element;
    this.props.getRef(element);
  }

  calcSlideData(props) {
    const { children, activeSlide } = props;

    this.slidesCount = React.Children.count(children);
    this.slideWidth = this._wrapper.offsetWidth;
    this.slidesContainerWidth = this.slidesCount * this.slideWidth;

    this.changeActiveSlide(activeSlide);
  }

  changeActiveSlide(currentSlideIndex) {
    if (currentSlideIndex < 0) {
      currentSlideIndex = 0;
    } else if (currentSlideIndex >= this.slidesCount) {
      currentSlideIndex = this.slidesCount - 1;
    }

    const currentSlideHeight = _.get(
      this,
      ['_slides', currentSlideIndex, 'offsetHeight'],
      0,
    );

    this.setState({
      currentSlideIndex,
      wrapperHeight: currentSlideHeight,
      slidesContainerLeftOffset: currentSlideIndex * this.slideWidth,
    });
  }

  get slides() {
    const { children } = this.props;

    const styles = {
      width: this.slideWidth,
    };

    return _.map(children, (child, index) => (
      <div ref={element => (this._slides[index] = element)}
           key={index}
           style={styles}>
        {child}
      </div>
    ));
  }

  render() {
    const { className } = this.props;
    const { wrapperHeight, slidesContainerLeftOffset } = this.state;

    const wrapperStyles = {
      height: `${wrapperHeight}px`,
    };
    const slidesContainerStyles = {
      width: `${this.slidesContainerWidth}px`,
      marginLeft: `-${slidesContainerLeftOffset}px`,
    };

    return (
      <div ref={this.saveRefWrapper}
           style={wrapperStyles}
           className={className}
           styleName="slider-wrapper">
        <div ref={element => (this._slidesContainer = element)}
             style={slidesContainerStyles}
             styleName="slides-container">
          {this.slides}
        </div>
      </div>
    );
  }
}
