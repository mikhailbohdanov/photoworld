import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class Step extends Component {
  static propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    getPrepareRef: PropTypes.func,
    prepareSkip: PropTypes.func,
    prepareStep: PropTypes.func,
  };

  static defaultProps = {
    getPrepareRef: _.noop,
    prepareSkip: Promise.reject.bind(Promise),
    prepareStep: Promise.resolve.bind(Promise),
  };

  constructor(props) {
    super(props);

    const { getPrepareRef, prepareStep, prepareSkip } = props;
    getPrepareRef(prepareStep, prepareSkip);
  }

  render() {
    const { className, children } = this.props;

    return (
      <div className={className}>
        {children}
      </div>
    );
  }
}
