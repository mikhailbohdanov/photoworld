import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import {
  RaisedButton,
  CircularProgress,
} from 'material-ui';

import {
  Stepper as MaterialStepper,
  Step as MaterialStep,
  StepLabel as MaterialLabel,
} from 'material-ui/Stepper';

import { SliderCore } from 'views/components/slider';

import styles from './stepper.styl';

import sharedTranslations from 'views/translations';


export const NEXT_BUTTON_TYPES = {
  DEFAULT: 'DEFAULT',
  DISABLED: 'DISABLED',
  SKIP: 'SKIP',
};

@ConnectAll(null, null, styles)
export default class Stepper extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element),
    ]),
    finishStep: PropTypes.element,
    onFinish: PropTypes.func,
    nextButtonText: PropTypes.any,
    skipButtonText: PropTypes.any,
    finishButtonText: PropTypes.any,
  };

  static defaultProps = {
    onFinish: _.noop,
    disablePrevButton: false,
    nextButtonText: <FormattedMessage {...sharedTranslations.nextStep} />,
    skipButtonText: <FormattedMessage {...sharedTranslations.skipStep} />,
    finishButtonText: <FormattedMessage {...sharedTranslations.finish} />,
  };

  state = {
    currentStepIndex: 0,
    finish: false,
    done: false,
    isFetching: false,
    nextButtonType: NEXT_BUTTON_TYPES.DEFAULT,
  };

  componentDidMount() {
    this.initStepper(this.props);
  }

  componentWillUpdate(nextProps) {
    if (this.props.children !== nextProps.children) {
      this.initStepper(nextProps);
    }
  }

  stepsCount = 0;
  _prepareSteps = {};

  @autobind
  setFetching(isFetching = false) {
    this.setState({
      isFetching,
    });
  }

  savePrepareStepRef(index) {
    return (prepareStep, prepareSkip) => (this._prepareSteps[index] = {
      prepareStep,
      prepareSkip,
    });
  }

  @autobind
  setNextButtonType(buttonType) {
    this.setState({
      nextButtonType: buttonType,
    });
  }

  initStepper(props) {
    const { children } = props;

    this.stepsCount = React.Children.count(children);

    this.skipSteps();
  }

  @autobind
  skipSteps() {
    const { currentStepIndex } = this.state;
    const currentStep = _.get(this, ['_prepareSteps', currentStepIndex]);

    if (currentStep && currentStep.prepareSkip) {
      this.setFetching(true);

      currentStep.prepareSkip()
        .then(this.goToNextStep)
        .catch(this.setFetching);
    } else {
      this.setFetching(false);
    }
  }

  @autobind
  goToNextStep() {
    const { onFinish } = this.props;
    let { currentStepIndex } = this.state;
    currentStepIndex = currentStepIndex + 1;

    if (currentStepIndex >= this.stepsCount + 1) {
      onFinish();
    }

    this.setState({
      currentStepIndex,
      finish: currentStepIndex >= this.stepsCount,
      done: currentStepIndex >= this.stepsCount + 1,
      nextButtonText: NEXT_BUTTON_TYPES.DEFAULT,
    }, this.skipSteps);
  }

  @autobind
  prepareStep() {
    const { currentStepIndex, done } = this.state;

    if (done) {
      return;
    }

    this.setFetching(true);

    const prepareStep = _.get(
      this,
      ['_prepareSteps', currentStepIndex, 'prepareStep'],
      Promise.resolve.bind(Promise),
    );

    prepareStep()
      .then(this.goToNextStep)
      .catch(() => {
        this.setFetching(false);
      });
  }

  get steps() {
    return React.Children.map(this.props.children, (child, index) => (
      <MaterialStep key={index}>
        <MaterialLabel>{child.props.label}</MaterialLabel>
      </MaterialStep>
    ));
  }

  get finishStep() {
    const { finishStep } = this.props;

    if (!finishStep) {
      return null;
    }

    return (
      <div styleName="finish-step">
        {finishStep}
      </div>
    );
  }

  get controls() {
    const {
      nextButtonText,
      skipButtonText,
      finishButtonText,
    } = this.props;
    const {
      finish,
      isFetching,
      done,
      nextButtonType,
    } = this.state;

    const disabled = isFetching || done || nextButtonType === NEXT_BUTTON_TYPES.DISABLED;
    let nextStepButtonLabelText;
    if (finish) {
      nextStepButtonLabelText = finishButtonText;
    } else if (nextButtonType === NEXT_BUTTON_TYPES.SKIP) {
      nextStepButtonLabelText = skipButtonText;
    } else {
      nextStepButtonLabelText = nextButtonText;
    }

    return (
      <div styleName="buttons-wrapper">
        <RaisedButton primary
                      disabled={disabled}
                      label={nextStepButtonLabelText}
                      onTouchTap={this.prepareStep} />
      </div>
    );
  }

  get fetcher() {
    if (!this.state.isFetching) {
      return null;
    }

    return (
      <div styleName="spinner">
        <CircularProgress />
      </div>
    );
  }

  render() {
    const { className, children } = this.props;
    const { currentStepIndex } = this.state;

    return (
      <div className={className}
           styleName="wrapper">
        {this.fetcher}

        <MaterialStepper activeStep={currentStepIndex}>
          {this.steps}
        </MaterialStepper>

        <SliderCore activeSlide={currentStepIndex}>
          {React.Children.map(children, (child, index) => React.cloneElement(child, {
            getPrepareRef: this.savePrepareStepRef(index),
            setNextButtonType: this.setNextButtonType,
            activeStep: currentStepIndex === index,
          }))}
          {this.finishStep}
        </SliderCore>

        {this.controls}
      </div>
    );
  }
}
