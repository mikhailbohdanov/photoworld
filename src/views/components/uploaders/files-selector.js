import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';

import { RaisedButton } from 'material-ui';

import { ALL } from 'core/constants/files/mime-types';


export class FilesSelector extends Component {
  static propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    accept: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.string),
    ]),
    multiple: PropTypes.bool,
    onSelect: PropTypes.func,
  };

  static defaultProps = {
    accept: ALL,
    multiple: false,
    onSelect: _.noop,
  };

  @autobind
  handleInputChange() {
    this.props.onSelect(this._input.files);
  }

  get input() {
    const { multiple } = this.props;

    return (
      <input ref={input => this._input = input}
             style={{ display: 'none' }}
             type="file"
             multiple={multiple}
             accept={this.accept}
             onChange={this.handleInputChange} />
    );
  }

  get accept() {
    const { accept } = this.props;

    if (!accept) {
      return ALL;
    } else if (Array.isArray(accept)) {
      return accept.join(',');
    }

    return accept;
  }

  render() {
    const { label, className } = this.props;

    return (
      <RaisedButton label={label}
                    primary
                    className={className}
                    containerElement="label">
        {this.input}
      </RaisedButton>
    );
  }
}
