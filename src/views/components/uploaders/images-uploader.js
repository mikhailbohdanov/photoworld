import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';
import autobind from 'autobind-decorator';
import classNames from 'classnames';
import Cropper from 'react-cropper';

import ConnectAll from 'core/utils/connect-all';

import { IMAGES } from 'core/constants/files/mime-types';
import CROPPER_CONFIG from 'core/constants/modules-configs/cropper';

import { uploadPhotos } from 'core/redux/actions/images';

import {
  FilesSelector,
} from './index';

import styles from './images-uploader.styl';

import sharedTranslations from 'views/translations';


@ConnectAll(null, {
  uploadPhotos,
}, styles)
export class ImagesUploader extends Component {
  static propTypes = {
    className: PropTypes.string,
    multiple: PropTypes.bool,
    imageTypes: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.string),
    ]),
    sizes: PropTypes.object,
    enableCrop: PropTypes.bool,
    cropFactor: PropTypes.number,
    uploadPath: PropTypes.string,
    onSelectFiles: PropTypes.func,
    onUploadStart: PropTypes.func,
    onUploadFinish: PropTypes.func,
    getRef: PropTypes.func,
    uploadPhotos: PropTypes.func,
    updateProfileInfo: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    multiple: true,
    imageTypes: IMAGES,
    enableCrop: false,
    cropFactor: null,
    onSelectFiles: _.noop,
    onUploadStart: _.noop,
    onUploadFinish: _.noop,
    getRef: _.noop,
    uploadPhotos: _.noop,
    updateProfileInfo: _.noop,
  };

  state = {
    files: null,
  };

  componentWillMount() {
    this.props.getRef({
      startUpload: this.startUpload,
      reset: this.cleanSelectedFiles,
    });
  }

  @autobind
  startUpload() {
    const {
      uploadPath,
      onUploadStart,
      onUploadFinish,
      uploadPhotos,
    } = this.props;

    onUploadStart();

    uploadPhotos(uploadPath, this.state.files)
      .then(onUploadFinish);
  }

  @autobind
  onFilesSelected(files) {
    this.setState({
      files,
    });

    this.props.onSelectFiles(files);
  }

  @autobind
  cleanSelectedFiles() {
    this.setState({
      files: null,
    });

    this.props.onSelectFiles(null);
  }

  get filesSelector() {
    if (this.state.files) {
      return null;
    }

    const {
      multiple,
      imageTypes,
      intl: { formatMessage },
    } = this.props;

    // TODO fix this animation
    const styleNames = classNames('upload-button', {
      // pinned: this.state.files,
    });

    return (
      <FilesSelector styleName={styleNames}
                     label={formatMessage(sharedTranslations.choosePhoto)}
                     multiple={multiple}
                     accept={imageTypes}
                     onSelect={this.onFilesSelected} />
    );
  }

  get photoCrop() {
    const { multiple, cropFactor } = this.props;
    const { files } = this.state;

    if (!files || multiple) {
      return null;
    }

    const fileDataAsUrl = URL.createObjectURL(files[0]);

    return (
      <div styleName="cropper">
        <Cropper {...CROPPER_CONFIG}
                 aspectRatio={cropFactor}
                 src={fileDataAsUrl} />
      </div>
    );
  }

  get photoUploadStatus() {
    return null;
  }

  render() {
    const { className } = this.props;

    return (
      <div className={className}
           styleName="wrapper">
        {this.filesSelector}
        {this.photoCrop}
        {this.photoUploadStatus}
      </div>
    );
  }
}
