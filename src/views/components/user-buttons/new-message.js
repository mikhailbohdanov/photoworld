import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { intlShape } from 'react-intl';

import ConnectAll from 'core/utils/connect-all';

import { selectProfileInfo } from 'core/redux/selectors/profiles/profile';
import { getProfileInfo } from 'core/redux/actions/profiles/get';
import { sendMessage } from 'core/redux/actions/im/messages';
import { getFriendList } from 'core/redux/actions/profiles/lists/friends';

import {
  FlatButton,
  RaisedButton,
  TextField,
  SelectField,
  MenuItem,
} from 'material-ui';

import { openModal, closeModal } from 'views/components/modal';

import styles from './index.styl';

import sharedTranslations from 'views/translations';
import imTranslations from 'views/pages/im/translations';


@ConnectAll((state, { toUserId }) => ({
  profileInfo: selectProfileInfo(state, toUserId),
  profiles: state.profiles.list,
}), {
  getProfileInfo,
  sendMessage,
  getFriendList,
}, styles)
export class NewMessageButton extends Component {
  static propTypes = {
    toUserId: PropTypes.string,
    ButtonWrapper: PropTypes.any,
    profileInfo: PropTypes.object,
    getProfileInfo: PropTypes.func,
    sendMessage: PropTypes.func,
    getFriendList: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    ButtonWrapper: RaisedButton,
    getProfileInfo: _.noop,
    sendMessage: _.noop,
    getFriendList: _.noop,
  };

  constructor(props) {
    super(props);

    const { toUserId, getFriendList } = this.props;

    if (!toUserId) {
      getFriendList();
    }

    this.state = {
      toUserId,
      messageText: null,
    };
  }

  componentWillMount() {
    const { getProfileInfo } = this.props;
    const { toUserId } = this.state;

    getProfileInfo(toUserId);
  }

  @autobind
  openNewMessage() {
    const {
      intl: { formatMessage },
    } = this.props;

    openModal((
      <div className={styles.wrapper}>
        {this.friendList}

        <TextField className={styles['full-width']}
                   hintText={formatMessage(imTranslations.enterText)}
                   multiLine={true}
                   rows={1}
                   rowsMax={6}
                   onChange={this.handleChangeMessage}
                   onKeyDown={this.handleButtonsPress} />
      </div>
    ), {
      title: this.title,
      actions: this.actionButtons,
    });
  }

  @autobind
  handleChangeMessage(event, messageText) {
    this.setState({
      messageText,
    });
  }

  @autobind
  handleButtonsPress(event) {
    if (event.ctrlKey && event.key === 'Enter') {
      this.sendMessage();
    }
  }

  @autobind
  sendMessage() {
    const {
      toUserId,
      messageText,
    } = this.state;

    if (toUserId && messageText) {
      this.props.sendMessage(toUserId, messageText)
        .then(closeModal);
    }
  }

  @autobind
  handleChange(event, profileId) {
    this.setState({
      toUserId: profileId,
    });
  }

  get friendList() {
    if (this.props.toUserId) {
      return null;
    }

    return (
      <SelectField value={this.state.toUserId}
                   onChange={this.handleChange}
                   maxHeight={200}>
        {_.map(this.props.profiles, (profile, profileId) => (
          <MenuItem key={profileId}
                    value={profileId}>
            {profile.firstName}
            {' '}
            {profile.lastName}
          </MenuItem>
        ))}
      </SelectField>
    );
  }

  get title() {
    const {
      profileInfo,
      intl: { formatMessage },
    } = this.props;

    if (profileInfo) {
      return formatMessage(imTranslations.sendMessageTo, profileInfo);
    }

    return formatMessage(imTranslations.newMessage);
  }

  get actionButtons() {
    const { intl: { formatMessage } } = this.props;

    return [
      <FlatButton key="send"
                  className={styles['action-button']}
                  primary
                  label={formatMessage(sharedTranslations.send)}
                  onTouchTap={this.sendMessage} />,
      <FlatButton key="cancel"
                  className={styles['action-button']}
                  label={formatMessage(sharedTranslations.cancel)}
                  onTouchTap={closeModal} />
    ];
  }

  render() {
    const { ButtonWrapper } = this.props;
    const props = _.pick(this.props, ['className', 'children']);

    return (
      <ButtonWrapper {...props}
                     onTouchTap={this.openNewMessage} />
    );
  }
}
