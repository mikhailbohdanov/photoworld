import PropTypes from 'prop-types';

import ConnectAll from 'core/utils/connect-all';

import {
  subscribe,
  unsubscribe,
} from 'core/redux/actions/im/dialogs';

import { RootDataLoader } from './index';


@ConnectAll(null, {
  subscribe,
  unsubscribe,
}, null)
export class DialogsLoader extends RootDataLoader {
  static propTypes = {
    subscribe: PropTypes.func,
    unsubscribe: PropTypes.func,
  };

  static defaultProps = {
    subscribe: _.noop,
    unsubscribe: _.noop,
  };

  fetchData() {
    this.props.subscribe();
  }

  unfetchData() {
    this.props.unsubscribe();
  }

}
