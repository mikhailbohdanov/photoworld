import { Component } from 'react';


export class RootDataLoader extends Component {

  componentWillMount() {
    this.fetchData();
  }

  componentWillUnmount() {
    this.unfetchData();
  }

  // eslint-disable-next-line lodash/prefer-constant
  fetchData() {
    return null;
  }

  // eslint-disable-next-line lodash/prefer-constant
  unfetchData() {
    return null;
  }

  // eslint-disable-next-line lodash/prefer-constant
  reset() {
    return null;
  }

  // eslint-disable-next-line lodash/prefer-constant
  render() {
    return null;
  }
}
