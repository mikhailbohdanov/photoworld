import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Route } from 'react-router-dom';
import autobind from 'autobind-decorator';
import { intlShape, FormattedMessage } from 'react-intl';
import Cookie from 'js-cookie';

import ConnectAll from 'core/utils/connect-all';

import { signOut } from 'core/redux/actions/auth/signout';

import { isAuthenticated } from 'core/redux/selectors/auth';
import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import {
  MuiThemeProvider,
  AppBar,
  FlatButton,
  IconButton,
  IconMenu,
  MenuItem,
} from 'material-ui';

import ActionCameraEnhanceIcon from 'material-ui/svg-icons/action/camera-enhance';
import ActionAccountCircleIcon from 'material-ui/svg-icons/action/account-circle';
import ActionExitToAppIcon from 'material-ui/svg-icons/action/exit-to-app';
import ActionSettingsIcon from 'material-ui/svg-icons/action/settings';
import NavigationMoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import CommunicationChatIcon from 'material-ui/svg-icons/communication/chat';

import { Modal } from 'views/components/modal';

import styles from './app-layout.styl';

import sharedTranslations from 'views/translations';
import translations from './translations';

// - DataLoaders
import { DialogsLoader } from 'views/data-loaders/dialogs-loader';

// - Routes
import MainPage from 'views/pages/main';
import SignIn from 'views/pages/auth/signin';
import SignUp from 'views/pages/auth/signup';

import IM from 'views/pages/im';

import ViewProfile from 'views/pages/profile/view';


@withRouter
@ConnectAll(state => ({
  isAuthenticated: isAuthenticated(state),
  currentUserId: selectCurrentProfile(state, 'uid'),
}), {
  signOut,
}, styles)
export default class AppLayout extends Component {
  static propTypes = {
    history: PropTypes.object,
    children: PropTypes.element,
    isAuthenticated: PropTypes.bool,
    currentUserId: PropTypes.string,
    signOut: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    signOut: _.noop,
  };

  state = {
    openUserMenu: false,
  };

  goToUrl(url) {
    this.props.history.push(url);
  }

  @autobind
  goToMain() {
    this.goToUrl('/');
  }

  @autobind
  goToSignIn() {
    this.goToUrl('/sign-in');
  }

  @autobind
  changeLocation(item, location) {
    this.goToUrl(location);
  }

  @autobind
  signOut() {
    this.props.signOut()
      .then(() => {
        this.goToMain();
      });
  }

  get header() {
    return (
      <AppBar title={<FormattedMessage {...sharedTranslations.world} />}
              iconElementLeft={
                <IconButton onTouchTap={this.goToMain}>
                  <ActionCameraEnhanceIcon />
                </IconButton>
              }
              iconElementRight={this.headerMenu} />
    );
  }

  get headerMenu() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated) {
      return this.userMenu;
    }

    return (
      <FlatButton onTouchTap={this.goToSignIn}>
        <FormattedMessage {...sharedTranslations.signIn} />
      </FlatButton>
    );
  }

  get userMenu() {
    const { currentUserId } = this.props;

    return (
      <div>
        <IconMenu iconButtonElement={<IconButton><NavigationMoreVertIcon /></IconButton>}
                  onChange={this.changeLocation}>
          <MenuItem leftIcon={<ActionAccountCircleIcon />}
                    value={`/profile/${currentUserId}`}>
            <FormattedMessage {...translations.myProfile} />
          </MenuItem>
          <MenuItem leftIcon={<CommunicationChatIcon />}
                    value="/im">
            <FormattedMessage {...translations.messages} />
          </MenuItem>
          <MenuItem leftIcon={<ActionSettingsIcon />}
                    value="/settings">
            <FormattedMessage {...translations.settings} />
          </MenuItem>
          <MenuItem leftIcon={<ActionExitToAppIcon />}
                    onTouchTap={this.signOut}>
            <FormattedMessage {...translations.signOut} />
          </MenuItem>
        </IconMenu>
      </div>
    );
  }

  selectLang(lang) {
    Cookie.set('locale', lang);
    window.location.reload();
  }

  get footer() {
    return (
      <div>
        <a href="#" onClick={() => this.selectLang('en')}>
          English
        </a>
        {' '}
        <a href="#" onClick={() => this.selectLang('ru')}>
          Русский
        </a>
      </div>
    );
  }

  render() {
    return (
      <MuiThemeProvider>
        <div styleName="layout-wrapper">
          <DialogsLoader />

          {this.header}

          <div styleName="routes">
            <Route exact path="/" component={MainPage} />

            <Route path="/sign-in" component={SignIn} />
            <Route path="/sign-up" component={SignUp} />

            <Route path="/im" component={IM} />

            <Route exact path="/profile" component={ViewProfile} />
            <Route path="/profile/:userId" component={ViewProfile} />
          </div>

          {this.footer}

          <Modal />
        </div>
      </MuiThemeProvider>
    );
  }
}
