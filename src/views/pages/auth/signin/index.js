import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import { isFetching, selectErrors } from 'core/redux/selectors';
import { signInEmail } from 'core/redux/actions/auth/email';
import { connectFacebook } from 'core/redux/actions/auth/facebook';
import { connectGoogle } from 'core/redux/actions/auth/google';
import { getProfileInfo } from 'core/redux/actions/profiles/get';

import {
  Paper,
  TextField,
  RaisedButton,
} from 'material-ui';

import {
  DelimiterHorizontal,
  DelimiterVertical,
  DelimiterContent,
} from 'views/components/delimiter';
import Link from 'views/components/link';
import { CenterFetcher } from 'views/components/fetcher';

import styles from './index.styl';

import sharedTranslations from 'views/translations';
import authTranslations from '../translations';


@withRouter
@ConnectAll(state => ({
  isFetching: isFetching(state, 'auth'),
  errors: selectErrors(state, 'auth'),
}), {
  signInEmail,
  connectFacebook,
  connectGoogle,
  getProfileInfo,
}, styles)
export default class SignIn extends Component {
  static propTypes = {
    errors: PropTypes.object,
    isFetching: PropTypes.bool,
    signInEmail: PropTypes.func,
    connectFacebook: PropTypes.func,
    connectGoogle: PropTypes.func,
    getProfileInfo: PropTypes.func,
    history: PropTypes.object,
    intl: intlShape,
  };

  static defaultProps = {
    signInEmail: _.noop,
    connectFacebook: _.noop,
    connectGoogle: _.noop,
    getProfileInfo: _.noop,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  state = {
    email: '',
    password: '',
    errors: null,
  };

  @autobind
  handleSignInAccount() {
    const { signInEmail } = this.props;
    const email = this._email.input.value;
    const password = this._password.input.value;

    signInEmail(email, password)
      .then(this.goToProfile)
      .catch(errors => {
        this.setState({
          errors,
        });
      });
  }

  @autobind
  goToProfile(user) {
    this.props.history.push(`/profile/${user.uid}`);
  }

  @autobind
  checkProfileFinished(user) {
    this.props.getProfileInfo(user.uid)
      .then(({ isCompleted }) => {
        if (isCompleted) {
          this.goToProfile(user.uid);
        } else {
          this.props.history.push('/sign-up');
        }
      });
  }

  get signInBlock() {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <DelimiterContent styleName="delimiter-content-v">
        {this.email}
        {this.password}

        <div styleName="buttons-wrapper">
          <RaisedButton primary
                        label={formatMessage(sharedTranslations.signIn)}
                        onTouchTap={this.handleSignInAccount} />
        </div>
      </DelimiterContent>
    );
  }

  get email() {
    const { intl: { formatMessage } } = this.props;
    const { errors } = this.state;
    const error = _.has(errors, 'email') ? formatMessage(_.get(errors, 'email')) : null;

    return (
      <TextField ref={component => this._email = component}
                 floatingLabelText={formatMessage(sharedTranslations.email)}
                 errorText={error} />
    );
  }

  get password() {
    const { intl: { formatMessage } } = this.props;
    const { errors } = this.state;
    const error = _.has(errors, 'password') ? formatMessage(_.get(errors, 'password')) : null;

    return (
      <TextField ref={component => this._password = component}
                 type="password"
                 floatingLabelText={formatMessage(sharedTranslations.password)}
                 errorText={error} />
    );
  }
  get signUpBlock() {
    return (
      <DelimiterContent styleName="delimiter-content-v">
        <Link className=""
              to="/sign-up">
          Create account
        </Link>
      </DelimiterContent>
    );
  }

  get socialButtonsBlock() {
    const {
      connectFacebook,
      connectGoogle,
      intl: { formatMessage },
    } = this.props;

    return (
      <DelimiterContent styleName="delimiter-content-h auth-with-social">
        <RaisedButton primary
                      styleName="social-button facebook"
                      label={formatMessage(authTranslations.signInWithFacebook)}
                      onTouchTap={() => connectFacebook().then(this.checkProfileFinished)} />

        <RaisedButton primary
                      styleName="social-button google"
                      label={formatMessage(authTranslations.signInWithGoogle)}
                      onTouchTap={() => connectGoogle().then(this.checkProfileFinished)} />

      </DelimiterContent>
    );
  }

  get fetcher() {
    if (!this.props.isFetching) {
      return null;
    }

    return <CenterFetcher />;
  }

  render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <div styleName="wrapper">
        {this.fetcher}

        <Paper zDepth={2}>
          <DelimiterHorizontal styleName="delimiter-h"
                               delimiterText={formatMessage(sharedTranslations.or)}>
            <DelimiterContent styleName="delimiter-content-h">
              <DelimiterVertical delimiterText={formatMessage(sharedTranslations.or)}>
                {this.signInBlock}
                {this.signUpBlock}
              </DelimiterVertical>
            </DelimiterContent>

            {this.socialButtonsBlock}
          </DelimiterHorizontal>
        </Paper>
      </div>
    );
  }
}
