import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import { updateProfileInfo } from 'core/redux/actions/profiles/update';

import { Paper } from 'material-ui';

import { Stepper } from 'views/components/stepper';

import {
  Step1,
  Step2,
  Step3,
} from './steps';

import styles from './index.styl';

import translations from '../translations';


@withRouter
@ConnectAll(null, {
  updateProfileInfo,
}, styles)
export default class SignUp extends Component {
  static propTypes = {
    history: PropTypes.object,
    updateProfileInfo: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    updateProfileInfo: _.noop,
  };

  @autobind
  onFinish() {
    const { history } = this.props;

    updateProfileInfo({

    })
      .then(() => {
        history.push('/');
      })
      .catch(() => {
        history.push('/');
      });
  }

  get finishStep() {
    return (
      <div className={styles['finish-step']}>
        <h1>Thanks for register!</h1>
      </div>
    );
  }

  render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <div styleName="wrapper">
        <Paper zDepth={2}>
          <Stepper finishStep={this.finishStep}
                   onFinish={this.onFinish}>
            <Step1 label={formatMessage(translations.createAccount)} />
            <Step2 label={formatMessage(translations.fillInfo)} />
            <Step3 label={formatMessage(translations.uploadPhoto)} />
          </Stepper>
        </Paper>
      </div>
    );
  }
}
