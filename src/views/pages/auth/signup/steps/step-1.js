import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';

import ConnectAll from 'core/utils/connect-all';

import { isAuthenticated } from 'core/redux/selectors/auth';
import { signUpEmail } from 'core/redux/actions/auth/email';

import {
  TextField,
} from 'material-ui';

import { Step } from 'views/components/stepper';

import styles from './steps.styl';

import sharedTranslations from 'views/translations';


@ConnectAll(state => ({
  isAuthenticated: isAuthenticated(state),
}), {
  signUpEmail,
}, styles)
export default class Step1 extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool,
    signUpEmail: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    signUpEmail: _.noop,
  };

  state = {
    email: '',
    password: '',
    retryPassword: '',
    errors: null,
  };

  @autobind
  handleChange(key) {
    return (event, value) => {
      this.setState({
        [key]: value,
      });
    };
  }

  @autobind
  handleSkipStep() {
    return this.props.isAuthenticated ?
      Promise.resolve(null) :
      Promise.reject(null);
  }

  @autobind
  handleSignInAccount() {
    const { signUpEmail } = this.props;
    const {
      email,
      password,
      retryPassword,
    } = this.state;

    this.setState({
      errors: null,
    });

    return signUpEmail(email, password, retryPassword)
      .catch(errors => {
        this.setState({
          errors,
        });

        throw errors._origin;
      });
  }

  get email() {
    const { intl: { formatMessage } } = this.props;
    const { errors } = this.state;
    const error = _.has(errors, 'email') ? formatMessage(_.get(errors, 'email')) : null;

    return (
      <div styleName="input-field full">
        <TextField onChange={this.handleChange('email')}
                   floatingLabelText={formatMessage(sharedTranslations.email)}
                   errorText={error} />
      </div>
    );
  }

  get password() {
    const { intl: { formatMessage } } = this.props;
    const { errors } = this.state;
    const error = _.has(errors, 'password') ? formatMessage(_.get(errors, 'password')) : null;

    return (
      <div styleName="input-field full">
        <TextField onChange={this.handleChange('password')}
                   floatingLabelText={formatMessage(sharedTranslations.password)}
                   type="password"
                   errorText={error} />
      </div>
    );
  }

  get retryPassword() {
    const { intl: { formatMessage } } = this.props;
    const { errors } = this.state;
    const error = _.has(errors, 'retryPassword') ? formatMessage(_.get(errors, 'retryPassword')) : null;

    return (
      <div styleName="input-field full">
        <TextField onChange={this.handleChange('retryPassword')}
                   floatingLabelText={formatMessage(sharedTranslations.retryPassword)}
                   type="password"
                   errorText={error} />
      </div>
    );
  }

  render() {
    return (
      <Step {...this.props}
            prepareSkip={this.handleSkipStep}
            prepareStep={this.handleSignInAccount}>
        <div styleName="input-fields">
          {this.email}
          {this.password}
          {this.retryPassword}
        </div>
      </Step>
    );
  }
}
