import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';
import { intlShape, FormattedMessage } from 'react-intl';

import ConnectAll from 'core/utils/connect-all';

import { getProfileInfo } from 'core/redux/actions/profiles/get';
import { fillProfileInfo } from 'core/redux/actions/profiles/signup/fill-user-info';

import {
  TextField,
  DatePicker,
} from 'material-ui';

import { Step } from 'views/components/stepper';

import styles from './steps.styl';

import sharedTranslations from 'views/translations';
import authTranslations from '../../translations';


const INPUT_FIELDS = [
  'firstName', 'lastName', 'birthDay', 'city',
  'phone', 'email', 'website', 'interests',
  'aboutMe',
];


@ConnectAll(() => ({}), {
  getProfileInfo,
  fillProfileInfo,
}, styles)
export default class Step2 extends Component {
  static propTypes = {
    getProfileInfo: PropTypes.func,
    fillProfileInfo: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    getProfileInfo: () => Promise.reject(null),
    fillProfileInfo: _.noop,
  };

  state = {
    firstName: '',
    lastName: '',
    birthDay: null,
    aboutMe: '',
    city: null,
    phone: null,
    email: null,
    website: null,
    interests: null,
    errors: null,
  };

  @autobind
  handleChange(key) {
    return (event, value) => {
      this.setState({
        [key]: value,
      });
    };
  }

  @autobind
  handleSkipStep() {
    return this.props.getProfileInfo();
  }

  @autobind
  saveUserInfo() {
    const { fillProfileInfo } = this.props;
    const fields = _.pick(this.state, INPUT_FIELDS);

    this.setState({
      errors: null,
    });

    return fillProfileInfo(fields)
      .catch(errors => {
        this.setState({
          errors,
        });

        throw errors._origin;
      });
  }

  render() {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <Step {...this.props}
            prepareSkip={this.handleSkipStep}
            prepareStep={this.saveUserInfo}>
        <div styleName="input-fields">
          <div styleName="input-delimiter">
            <FormattedMessage {...authTranslations.mainInfo} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('firstName')}
                       floatingLabelText={formatMessage(sharedTranslations.firstName)} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('lastName')}
                       floatingLabelText={formatMessage(sharedTranslations.lastName)} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('city')}
                       floatingLabelText={formatMessage(sharedTranslations.city)} />
          </div>
          <div styleName="input-field half">
            <DatePicker onChange={this.handleChange('birthDay')}
                        floatingLabelText={formatMessage(sharedTranslations.birthDay)}
                        mode="landscape" />
          </div>

          <div styleName="input-delimiter">
            <FormattedMessage {...authTranslations.contacts} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('phone')}
                       floatingLabelText={formatMessage(sharedTranslations.phone)} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('email')}
                       floatingLabelText={formatMessage(sharedTranslations.email)} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('website')}
                       floatingLabelText={formatMessage(sharedTranslations.website)} />
          </div>

          <div styleName="input-delimiter">
            <FormattedMessage {...authTranslations.moreInfo} />
          </div>
          <div styleName="input-field half">
            <TextField onChange={this.handleChange('aboutMe')}
                       floatingLabelText={formatMessage(sharedTranslations.aboutMe)}
                       multiLine
                       rows={2}
                       rowsMax={4} />
          </div>
        </div>
      </Step>
    );
  }
}
