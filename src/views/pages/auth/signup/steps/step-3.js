import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import {
  Step,
  NEXT_BUTTON_TYPES,
} from 'views/components/stepper';
import { ProfilePhoto } from '../../../profile/components/profile-photo'

import styles from './steps.styl';


@ConnectAll(null, null, styles)
export default class Step3 extends Component {
  static propTypes = {
    uploadProfilePhoto: PropTypes.func,
    setNextButtonType: PropTypes.func,
    activeStep: PropTypes.bool,
  };

  static defaultProps = {
    uploadProfilePhoto: _.noop,
    setNextButtonType: _.noop,
  };

  componentWillUpdate(nextProps) {
    const { activeStep } = this.props;
    if (activeStep !== nextProps.activeStep && nextProps.activeStep) {
      this.props.setNextButtonType(NEXT_BUTTON_TYPES.SKIP);
    }
  }

  @autobind
  prepareStep() {
    return new Promise((resolve, reject) => {
      setTimeout(resolve, 1000);
    });
  }

  render() {
    return (
      <Step {...this.props}
            prepareStep={this.prepareStep}>
        <div styleName="content-wrapper">
          <ProfilePhoto styleName="photo-uploader"
                        onDone={this.loaded} />
        </div>
      </Step>
    );
  }
}
