import { defineMessages } from 'react-intl';


export default defineMessages({

  // Common
  mainInfo: {
    id: 'auth.common.main-info',
    defaultMessage: 'Main info',
  },
  contacts: {
    id: 'auth.common.contacts',
    defaultMessage: 'Contacts',
  },
  moreInfo: {
    id: 'auth.common.more-info',
    defaultMessage: 'More info',
  },

  // Auth with socials
  signInWithFacebook: {
    id: 'auth.sign-in.with-facebook',
    defaultMessage: 'Sign in with Facebook',
  },
  signInWithGoogle: {
    id: 'auth.sign-in.with-google',
    defaultMessage: 'Sign in with Google',
  },

  // Signup steps
  createAccount: {
    id: 'auth.sign-up.steps.create-account',
    defaultMessage: 'Create account',
  },
  fillInfo: {
    id: 'auth.sign-up.steps.fill-info',
    defaultMessage: 'Fill info',
  },
  uploadPhoto: {
    id: 'auth.sign-up.steps.upload-photo',
    defaultMessage: 'Upload photo',
  },

});
