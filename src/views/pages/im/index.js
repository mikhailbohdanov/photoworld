import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import DialogList from './list';
import DialogView from './view';


export default class IM extends Component {
  render() {
    return (
      <div>
        <Route exact path="/im" component={DialogList} />
        <Route path="/im/:userId" component={DialogView} />
      </div>
    );
  }
}
