import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, NavLink } from 'react-router-dom';
import autobind from 'autobind-decorator';
import { intlShape } from 'react-intl';

import ConnectAll from 'core/utils/connect-all';

import { dateTime2Friendly } from 'core/utils/date-time/formats';

import { selectDialog } from 'core/redux/selectors/im/dialogs';
import { selectProfileInfo } from 'core/redux/selectors/profiles/profile';

import { remove } from 'core/redux/actions/im/dialogs';
import { getProfileInfo } from 'core/redux/actions/profiles';


import {
  Card,
  CardHeader,
  CardText,
  CardActions,
  RaisedButton,
} from 'material-ui';

import Avatar from 'views/components/avatar';
import styles from './index.styl';

import imTranslations from '../../../translations';


@withRouter
@ConnectAll((state, { userId }) => ({
  dialog: selectDialog(state, userId),
  profileInfo: selectProfileInfo(state, userId),
}), {
  remove,
  getProfileInfo,
}, styles)
export default class Dialog extends Component {
  static propTypes = {
    className: PropTypes.string,
    userId: PropTypes.string,
    dialog: PropTypes.object,
    profileInfo: PropTypes.object,
    getDialog: PropTypes.func,
    removeDialog: PropTypes.func,
    getProfileInfo: PropTypes.func,
    history: PropTypes.object,
    intl: intlShape,
  };

  static defaultProps = {
    removeDialog: () => Promise.reject(null),
    getProfileInfo: () => Promise.reject(null),
  };

  state = {
    isFetching: true,
  };

  componentWillMount() {
    const {
      userId,
      getProfileInfo,
    } = this.props;

    getProfileInfo(userId)
      .then(() => {
        this.setState({
          isFetching: false,
        });
      });
  }

  @autobind
  goToDialog() {
    const { history, userId } = this.props;
    history.push(`/im/${userId}`);
  }

  @autobind
  removeDialog() {
    const { removeDialog, userId } = this.props;
    removeDialog(userId);
  }

  get userName() {
    const {
      userId,
      profileInfo: { firstName, lastName },
    } = this.props;

    return (
      <NavLink className={styles['user-name']}
               to={`/profile/${userId}`}>
        {firstName}
        {' '}
        {lastName}
      </NavLink>
    );
  }

  get userPhoto() {
    const {
      userId,
      profileInfo: { photoUrl },
    } = this.props;

    return (
      <NavLink className={styles['user-photo']}
               to={`/profile/${userId}`}>
        <Avatar url={photoUrl}
                size={40} />
      </NavLink>
    );
  }

  get lastMessageTime() {
    return dateTime2Friendly(this.props.dialog.lastMessageTime);
  }

  get lastMessageText() {
    return (
      <CardText>
        {this.props.dialog.lastMessageText}
      </CardText>
    );
  }

  get actions() {
    const { intl: { formatMessage } } = this.props;

    return (
      <CardActions>
        <RaisedButton primary
                      label={formatMessage(imTranslations.openDialog)}
                      onTouchTap={this.goToDialog} />
        <RaisedButton label={formatMessage(imTranslations.removeDialog)}
                      onTouchTap={this.removeDialog} />
      </CardActions>
    );
  }

  render() {
    const { className, profileInfo, dialog } = this.props;

    if (this.state.isFetching || !profileInfo || !dialog) {
      return null;
    }

    return (
      <div className={className}>
        <Card>
          <CardHeader title={this.userName}
                      subtitle={this.lastMessageTime}
                      avatar={this.userPhoto} />

          {this.lastMessageText}
          {this.actions}
        </Card>
      </div>
    );
  }
}
