import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';

import ConnectAll from 'core/utils/connect-all';

import { selectDialogIds } from 'core/redux/selectors/im/dialogs';

import {
  Paper,
  IconButton,
} from 'material-ui';
import {
  Toolbar,
  ToolbarGroup,
} from 'material-ui/Toolbar';
import ContentAddIcon from 'material-ui/svg-icons/content/add';

import { NewMessageButton } from 'views/components/user-buttons';

import Dialog from './components/dialog';

import styles from './index.styl';


@ConnectAll(state => ({
  userIds: selectDialogIds(state),
}), null, styles)
export default class DialogList extends Component {
  static propTypes = {
    userIds: PropTypes.array,
    intl: intlShape,
    history: PropTypes.object,
  };

  get header() {
    return (
      <Toolbar>
        <ToolbarGroup>
          <NavLink styleName="breadcrump-link"
                   to="/im">
            Dialogs
          </NavLink>
        </ToolbarGroup>
        <ToolbarGroup>
          <NewMessageButton ButtonWrapper={IconButton}>
            <ContentAddIcon />
          </NewMessageButton>
        </ToolbarGroup>
      </Toolbar>
    );
  }

  get dialogList() {
    const { userIds } = this.props;

    if (!userIds || !userIds.length) {
      return this.empty;
    }

    return (
      <div styleName="dialog-list">
        {_.map(userIds, userId => (
          <Dialog key={userId}
                  styleName="dialog-item"
                  userId={userId} />
        ))}
      </div>
    );
  }

  get empty() {
    return (
      <div styleName="empty">
        You don't have dialogs at current moment!
      </div>
    );
  }

  render() {
    return (
      <div styleName="wrapper">
        <Paper zDepth={1}>
          {this.header}

          {this.dialogList}
        </Paper>
      </div>
    );
  }
}
