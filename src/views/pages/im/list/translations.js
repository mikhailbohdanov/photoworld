import { defineMessages } from 'react-intl';


export default defineMessages({
  sendMessageTo: {
    id: 'profile.view.send-message-to',
    defaultMessage: 'Send message to {firstName} {lastName}',
  },

});
