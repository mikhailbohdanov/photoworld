import { defineMessages } from 'react-intl';


export default defineMessages({
  search: {
    id: 'im.search',
    defaultMessage: 'Search',
  },
  enterSearchPhrase: {
    id: 'im.search.phrase',
    defaultMessage: 'Enter search phrase',
  },

  openDialog: {
    id: 'im.dialog.open',
    defaultMessage: 'Open dialog',
  },
  removeDialog: {
    id: 'im.dialog.remove',
    defaultMessage: 'Remove dialog',
  },

  enterText: {
    id: 'im.messages.enter-text',
    defaultMessage: 'Enter text',
  },
  send: {
    id: 'im.messages.send',
    defaultMessage: 'Send',
  },
  sendMessageTo: {
    id: 'im.messages.send-to',
    defaultMessage: 'Send to {firstName} {lastName}',
  },
  newMessage: {
    id: 'im.messages.new-message',
    defaultMessage: 'New message',
  },

});
