import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { dateTime2Friendly } from 'core/utils/date-time/formats';

import ConnectAll from 'core/utils/connect-all';

import { MESSAGE_DIRECTION } from 'core/constants/im';

import styles from './index.styl';


@ConnectAll(null, null, styles)
export default class Message extends Component {
  static propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    time: PropTypes.number,
    direction: PropTypes.oneOf([
      MESSAGE_DIRECTION.IN,
      MESSAGE_DIRECTION.OUT,
    ]),
    read: PropTypes.bool,
  };

  get title() {
    const { title } = this.props;

    if (!title) {
      return null;
    }

    return (
      <div styleName="title">
        {title}
      </div>
    );
  }

  get time() {
    return (
      <div styleName="time">
        {dateTime2Friendly(this.props.time)}
      </div>
    );
  }

  render() {
    const {
      text,
      direction,
      read,
    } = this.props;

    return (
      <div styleName={classNames('wrapper', {
        in: direction === MESSAGE_DIRECTION.IN,
        out: direction === MESSAGE_DIRECTION.OUT,
        'not-read': !read,
      })}>
        <div styleName="message">
          {this.title}
          {text}
        </div>

        {this.time}
      </div>
    );
  }
}
