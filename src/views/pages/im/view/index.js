import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import { selectDialog } from 'core/redux/selectors/im/dialogs';
import { selectMessageList } from 'core/redux/selectors/im/messages';
import { selectProfileInfo } from 'core/redux/selectors/profiles/profile';

import {
  subscribe,
  unsubscribe,
  sendMessage,
} from 'core/redux/actions/im/messages';
import { getProfileInfo } from 'core/redux/actions/profiles/get';

import {
  Paper,
  TextField,
  RaisedButton,
} from 'material-ui';
import {
  Toolbar,
  ToolbarGroup,
} from 'material-ui/Toolbar';

import Message from './components/message';

import styles from './index.styl';

import imTranslations from '../translations';


@ConnectAll((state, { match: { params: { userId } } }) => ({
  userId,
  profileInfo: selectProfileInfo(state, userId),
  dialog: selectDialog(state, userId),
  messageList: selectMessageList(state),
}), {
  getProfileInfo,
  subscribe,
  unsubscribe,
  sendMessage,
}, styles)
export default class DialogView extends Component {
  static propTypes = {
    userId: PropTypes.string,
    profileInfo: PropTypes.object,
    dialog: PropTypes.object,
    messageList: PropTypes.array,
    getProfileInfo: PropTypes.func,
    subscribe: PropTypes.func,
    unsubscribe: PropTypes.func,
    sendMessage: PropTypes.func,
    match: PropTypes.object,
    intl: intlShape,
  };

  static defaultProps = {
    getProfileInfo: _.noop,
    subscribe: _.noop,
    unsubscribe: _.noop,
    sendMessage: _.noop,
  };

  state = {
    messageText: '',
  };

  componentWillMount() {
    const { subscribe, getProfileInfo, userId } = this.props;

    getProfileInfo(userId);
    subscribe(userId);
  }

  componentWillUnmount() {
    this.props.unsubscribe();
  }

  @autobind
  sendMessage() {
    const { messageText } = this.state;

    if (!messageText) {
      return;
    }

    const { sendMessage, userId } = this.props;

    sendMessage(userId, this.state.messageText);

    this.setState({
      messageText: '',
    });
  }

  @autobind
  changeMessageText(event, messageText) {
    this.setState({
      messageText,
    });
  }

  @autobind
  inputButtonClick(event) {
    if (event.ctrlKey && event.key === 'Enter') {
      this.sendMessage();
    }
  }

  get header() {
    const { userId, profileInfo } = this.props;

    return (
      <Toolbar>
        <ToolbarGroup>
          <NavLink styleName="breadcrump-link"
                   to="/im">
            Dialogs
          </NavLink>
          >
          <NavLink styleName="breadcrump-link"
                   to={`/profile/${userId}`}>
            {profileInfo && `${profileInfo.firstName} ${profileInfo.lastName}`}
          </NavLink>
        </ToolbarGroup>
      </Toolbar>
    );
  }

  get messageList() {
    const { messageList } = this.props;

    return (
      <div styleName="message-list">
        {_.map(messageList, (message, index) => (
          <Message key={index}
                   {...message} />
        ))}
      </div>
    );
  }

  get messageTextInput() {
    const { formatMessage } = this.props.intl;

    return (
      <div styleName="input-wrapper">
        <TextField styleName="input"
                   multiLine
                   value={this.state.messageText}
                   hintText={formatMessage(imTranslations.enterText)}
                   onChange={this.changeMessageText}
                   onKeyDown={this.inputButtonClick} />

        <RaisedButton styleName="send-button"
                      label={formatMessage(imTranslations.send)}
                      onTouchTap={this.sendMessage} />
      </div>
    );
  }

  render() {
    return (
      <div styleName="wrapper">
        <Paper zDepth={1}>
          {this.header}
          {this.messageList}
          {this.messageTextInput}
        </Paper>
      </div>
    );
  }
}
