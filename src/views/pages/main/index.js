import React, { Component } from 'react';

import ConnectAll from 'core/utils/connect-all';

import {
  GridList,
  GridTile,
  IconButton,
  Paper,
} from 'material-ui';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

import styles from './index.styl';


const _styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
  },
};

const tilesData = [
  {
    img: 'http://www.material-ui.com/images/grid-list/00-52-29-429_640.jpg',
    title: 'Breakfast',
    author: 'jill111',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/burger-827309_640.jpg',
    title: 'Tasty burger',
    author: 'pashminu',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/camera-813814_640.jpg',
    title: 'Camera',
    author: 'Danson67',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/morning-819362_640.jpg',
    title: 'Morning',
    author: 'fancycrave1',
  },
];


@ConnectAll(null, null, styles)
export default class MainPage extends Component {
  render() {
    return (
      <div styleName="wrapper">
        <Paper styleName="block"
               zDepth={1}>
          <GridList style={_styles.gridList} cols={2.2}>
            {tilesData.map(tile => (
              <GridTile key={tile.img}
                        title={tile.title}
                        actionIcon={<IconButton><StarBorder color="white" /></IconButton>}
                        titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)">
                <img src={tile.img} />
              </GridTile>
            ))}
          </GridList>
        </Paper>
        <Paper styleName="block padding">
          <img src="http://startjob.com.ua/employers/images/178.jpg" alt="" />
          <p>Профессия фотографа одна из тех редких профессий, которая может позволить совмещать и работу и хобби. Или наоборот – сделать свое хобби своей работой.</p>
          <br />
          <p>Это творческая профессия, занимаясь которой необходимо для себя решить что важнее: деньги или искусство.</p>
          <br />
          <p>На сегодняшний день существует огромное количество разнообразных обучающих курсов, пройдя которые можно достаточно свободно научиться обращаться с техникой, тем более, что в наш век цифровых технологий, казалось бы, нет ничего проще: навел камеру, нажал кнопку – а дальше фотоаппарат все сделает сам. И в некоторых сферах профессиональной деятельности фотографов, таких как спортивный репортер, или папарацци, охотящийся за звездами, это действительно первостепенно.</p>
          <br />
          <p>Но для того, чтобы научиться делать качественные и уникальные снимки, важно не только хорошо уметь обращаться с фотоаппаратом, но и иметь достаточно много других навыков, в том числе и врожденное чувство красоты.</p>
          <br />
          <p>И, как мы уже писали выше, необходимо найти ту золотую середину, когда профессиональная деятельность фотографа будет приносить удовлетворение как материальное, так и моральное. Если преобладать будет стремление к материальному благосостоянию, то в погоне за деньгами можно растерять свой талант, а если наоборот – то такую деятельность нельзя будет назвать работой, а только хобби, которое приносит немного денег.</p>
        </Paper>
      </div>
    );
  }
}
