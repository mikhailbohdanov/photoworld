import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';
import autobind from 'autobind-decorator';

import ConnectAll from 'core/utils/connect-all';

import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import { updateProfileInfo } from 'core/redux/actions/profiles/update';

import {
  RaisedButton,
} from 'material-ui';

import { ImagesUploader } from 'views/components/uploaders';

import styles from './index.styl';

import sharedTranslations from 'views/translations';


@ConnectAll(state => ({
  currentProfileId: selectCurrentProfile(state, 'uid'),
}), {
  updateProfileInfo,
}, styles)
export class ProfilePhoto extends Component {
  static propTypes = {
    className: PropTypes.string,
    onDone: PropTypes.func,
    currentProfileId: PropTypes.string,
    updateProfileInfo: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    onDone: _.noop,
    updateProfileInfo: _.noop,
  };

  state = {
    imageSelected: false,
    uploadStarted: false,
  };

  @autobind
  selectFiles(files) {
    this.setState({
      imageSelected: Boolean(files),
    });
  }

  @autobind
  uploadStarted() {
    this.setState({
      uploadStarted: true,
    });
  }

  @autobind
  uploadFinished([ photo ]) {
    const { updateProfileInfo, onDone } = this.props;

    updateProfileInfo({
      photoUrl: photo.fullName,
    })
      .then(onDone);
  }

  get imageUploader() {
    const { currentProfileId } = this.props;

    return (
      <div styleName="uploader">
        <ImagesUploader {...this.props}
                        multiple={false}
                        enableCrop
                        cropFactor={1}
                        uploadPath={`/${currentProfileId}`}
                        onSelectFiles={this.selectFiles}
                        onUploadStart={this.uploadStarted}
                        onUploadFinish={this.uploadFinished}
                        preview="preview"
                        getRef={uploader => this._uploader = uploader} />
      </div>
    );
  }

  get controls() {
    const {
      intl: { formatMessage },
    } = this.props;
    const {
      imageSelected,
      uploadStarted,
    } = this.state;
    const disableButtons = !imageSelected || uploadStarted;

    return (
      <div styleName="controls">
        <div styleName="preview"
             id="preview" />

        <div styleName="buttons">
          <RaisedButton styleName="button"
                        primary
                        disabled={disableButtons}
                        label={formatMessage(sharedTranslations.upload)}
                        onTouchTap={() => this._uploader.startUpload()} />
          <RaisedButton styleName="button"
                        disabled={disableButtons}
                        label={formatMessage(sharedTranslations.reset)}
                        onTouchTap={() => this._uploader.reset()} />
        </div>
      </div>
    );
  }

  render() {
    const { className } = this.props;

    return (
      <div className={className}
           styleName="wrapper">
        {this.imageUploader}
        {this.controls}
      </div>
    );
  }
}
