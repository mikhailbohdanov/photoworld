import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import autobind from 'autobind-decorator';
import { intlShape, FormattedMessage } from 'react-intl';

import ConnectAll from 'core/utils/connect-all';

import { FRIEND_TYPE } from 'core/constants/lists/friends';

import {
  selectProfileInfo,
  selectProfileDetails,
} from 'core/redux/selectors/profiles/profile';
import { selectCurrentProfile } from 'core/redux/selectors/profiles';

import {
  getProfileInfo,
  getProfileDetails,
} from 'core/redux/actions/profiles/get';
import {
  addToFriend,
  checkUserType,
} from 'core/redux/actions/profiles/lists/friends';
import { updateProfileInfo } from 'core/redux/actions/profiles/update';

import {
  Paper,
  RaisedButton,
  IconButton,
  GridList,
  GridTile,
  Subheader,
} from 'material-ui';
import FileUploadIcon from 'material-ui/svg-icons/file/file-upload';
import ActionDeleteIcon from 'material-ui/svg-icons/action/delete';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

import Avatar from 'views/components/avatar';
import {
  NewMessageButton,
} from 'views/components/user-buttons';
import { openModal, closeModal } from 'views/components/modal';

import { ProfilePhoto } from '../components/profile-photo';

import styles from './index.styl';

import translations from './translations';


const _styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 450,
    overflowY: 'auto',
  },
};

const tilesData = [
  {
    img: 'http://www.material-ui.com/images/grid-list/00-52-29-429_640.jpg',
    title: 'Breakfast',
    author: 'jill111',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/burger-827309_640.jpg',
    title: 'Tasty burger',
    author: 'pashminu',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/camera-813814_640.jpg',
    title: 'Camera',
    author: 'Danson67',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/morning-819362_640.jpg',
    title: 'Morning',
    author: 'fancycrave1',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/hats-829509_640.jpg',
    title: 'Hats',
    author: 'Hans',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/honey-823614_640.jpg',
    title: 'Honey',
    author: 'fancycravel',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/vegetables-790022_640.jpg',
    title: 'Vegetables',
    author: 'jill111',
  },
  {
    img: 'http://www.material-ui.com/images/grid-list/water-plant-821293_640.jpg',
    title: 'Water plant',
    author: 'BkrmadtyaKarki',
  },
];


@withRouter
@ConnectAll((state, { match }) => ({
  profileInfo: selectProfileInfo(state, match.params.userId),
  profileDetails: selectProfileDetails(state),
  currentUserId: selectCurrentProfile(state, 'uid'),
}), {
  getProfileInfo,
  getProfileDetails,
  addToFriend,
  checkUserType,
  updateProfileInfo,
}, styles)
export default class ViewProfile extends Component {
  static propTypes = {
    match: PropTypes.object,
    currentUserId: PropTypes.string,
    profileInfo: PropTypes.object,
    profileDetails: PropTypes.object,
    getProfileInfo: PropTypes.func,
    getProfileDetails: PropTypes.func,
    addToFriend: PropTypes.func,
    checkUserType: PropTypes.func,
    updateProfileInfo: PropTypes.func,
    intl: intlShape,
  };

  static defaultProps = {
    getProfileInfo: _.noop,
    getProfileDetails: _.noop,
    addToFriend: _.noop,
    checkUserType: _.noop,
  };

  state = {
    isFetching: true,
    userType: FRIEND_TYPE.NO_FRIEND,
  };

  componentWillMount() {
    this.loadProfileData();
  }

  @autobind
  loadProfileData() {
    const {
      getProfileInfo,
      getProfileDetails,
      checkUserType,
      match: { params: { userId } },
    } = this.props;

    const promiseOfInfo = getProfileInfo(userId, true);
    const promiseOfDetails = getProfileDetails(userId);

    Promise.all([
      promiseOfInfo,
      promiseOfDetails,
    ])
      .then(() => {
        if (this.isMyProfile) {
          checkUserType(userId)
            .then(userType => {
              this.setState({
                userType,
              });
            });
        }

        this.setState({
          isFetching: false,
        });
      });
  }

  @autobind
  openUploadPhoto() {
    openModal((
      <ProfilePhoto className={styles.uploader}
                    onDone={() => {
                      this.loadProfileData();
                      this.forceUpdate();
                      this.closeUploadPhoto();
                    }} />
    ), {
      title: 'Upload new photo'
    });
  }

  @autobind
  closeUploadPhoto() {
    closeModal();
  }

  @autobind
  addToFriend() {
    const {
      addToFriend,
      match: { params: { userId } },
    } = this.props;

    addToFriend(userId);
  }

  @autobind
  removePhoto() {
    this.props.updateProfileInfo({
      photoUrl: '',
    })
      .then(() => {
        this.loadProfileData();
        this.forceUpdate();
      });
  }

  get isMyProfile() {
    const { match, currentUserId } = this.props;
    return currentUserId === match.params.userId;
  }

  get isMyFriend() {
    return this.state.userType === FRIEND_TYPE.FRIEND;
  }

  get background() {
    const { backgroundUrl } = this.props.profileDetails;

    if (!backgroundUrl) {
      return null;
    }

    return (
      <div styleName="background">
      </div>
    );
  }

  get userPhoto() {
    const { profileInfo: { photoUrl } } = this.props;

    return (
      <Avatar styleName="photo"
              url={photoUrl}
              border
              size={200}>
        {this.editPhotoButtons}
      </Avatar>
    );
  }

  get editPhotoButtons() {
    if (!this.isMyProfile) {
      return null;
    }

    let removeButton;
    if (this.props.profileInfo.photoUrl) {
      removeButton = (
        <IconButton onTouchTap={this.removePhoto}>
          <ActionDeleteIcon />
        </IconButton>
      );
    }

    return (
      <div>
        <IconButton onTouchTap={this.openUploadPhoto}>
          <FileUploadIcon />
        </IconButton>

        {removeButton}
      </div>
    );
  }

  get infoBlock() {
    const {
      profileInfo: {
        firstName,
        lastName,
      },
      profileDetails: {
        phone,
        email,
        website,
      },
    } = this.props;
    const isYou = this.isMyProfile ? (
      <span>
        (<FormattedMessage {...translations.isYou} />)
      </span>
    ) : null;

    return (
      <div styleName="info-block">
        <div styleName="name">
          {firstName}
          {' '}
          {lastName}
          {isYou}
        </div>
        <div styleName="status">

        </div>
        <div styleName="contacts">
          {_.map({ phone, email, website }, (value, key) => value && (
            <div key={key}
                 styleName="contact">
              <span>
                <FormattedMessage {...translations[key]} />:
              </span>

              {value}
            </div>
          ))}
        </div>
      </div>
    );
  }

  get userActions() {
    const { userId } = this.props.match.params;
    const isMyProfile = this.isMyProfile;

    let editProfile;
    let sendMessage;
    let friendButton;

    if (isMyProfile) {
      editProfile = (
        <RaisedButton styleName="action-button">
          <FormattedMessage {...translations.editProfile} />
        </RaisedButton>
      );
    }

    if (!isMyProfile) {
      sendMessage = (
        <NewMessageButton styleName="action-button"
                          toUserId={userId}>
          <FormattedMessage {...translations.sendMessage} />
        </NewMessageButton>
      );

      if (!this.isMyFriend) {
        friendButton = (
          <RaisedButton styleName="action-button"
                        onTouchTap={this.addToFriend}>
            <FormattedMessage {...translations.addToFriends} />
          </RaisedButton>
        );
      }
    }

    return (
      <div styleName="actions">
        {editProfile}
        {sendMessage}
        {friendButton}
      </div>
    );
  }

  get userDetails() {
    return (
      <div styleName="details">
        Details
      </div>
    );
  }

  render() {
    if (this.state.isFetching) {
      return null;
    }

    return (
      <div styleName="wrapper">
        {this.background}

        <div styleName="profile-info">
          <Paper styleName="block"
                 zDepth={1}>
            <div styleName="header">
              {this.userPhoto}
              {this.infoBlock}
            </div>

            <div styleName="main-block">
              {this.userActions}
            </div>
          </Paper>

          <Paper styleName="block"
                 zDepth={1}>
            <GridList cellHeight={180}
                      cols={4}>
              <Subheader>December</Subheader>

              {tilesData.map(tile => (
                <GridTile key={tile.img}
                          title={tile.title}
                          subtitle={<span>by <b>{tile.author}</b></span>}
                          actionIcon={<IconButton><StarBorder color="white" /></IconButton>}>
                  <img src={tile.img} />
                </GridTile>
              ))}
            </GridList>
          </Paper>
        </div>
      </div>
    );
  }
}
