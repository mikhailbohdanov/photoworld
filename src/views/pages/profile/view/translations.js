import { defineMessages } from 'react-intl';


export default defineMessages({
  sendMessage: {
    id: 'profile.view.send-message',
    defaultMessage: 'Send message',
  },
  sendMessageTo: {
    id: 'profile.view.send-message-to',
    defaultMessage: 'Send message to {firstName} {lastName}',
  },

  isYou: {
    id: 'profile.view.is-you',
    defaultMessage: 'This is you',
  },

  editProfile: {
    id: 'profile.view.edit-profile',
    defaultMessage: 'Edit profile',
  },

  addToFriends: {
    id: 'profile.view.add-to-friends',
    defaultMessage: 'Add to friends',
  },

  phone: {
    id: 'profile.view.phone',
    defaultMessage: 'Phone',
  },
  email: {
    id: 'profile.view.email',
    defaultMessage: 'Email',
  },
  website: {
    id: 'profile.view.website',
    defaultMessage: 'Website',
  },

});
