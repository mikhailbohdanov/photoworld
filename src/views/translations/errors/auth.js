import { defineMessages } from 'react-intl';


export default defineMessages({
  emailEmpty: {
    id: 'errors.auth.email.empty',
    defaultMessage: 'Email is empty',
  },
  emailInvalid: {
    id: 'errors.auth.email.invalid',
    defaultMessage: 'Email is not valid',
  },
  emailAlreadyInUse: {
    id: 'errors.auth.email.already-in-use',
    defaultMessage: 'Email already in use',
  },
  passwordEmpty: {
    id: 'errors.auth.password.empty',
    defaultMessage: 'Password is empty',
  },
  passwordEquals: {
    id: 'errors.auth.password.retry-not-equal',
    defaultMessage: 'Passwords not equals',
  },
  passwordInvalid: {
    id: 'errors.auth.password.invalid',
    defaultMessage: 'Password is not valid',
  },


  userNotFound: {
    id: 'errors.auth.user-not-found',
    defaultMessage: 'User with this email not found',
  },

});
