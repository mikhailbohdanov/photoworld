import { defineMessages } from 'react-intl';


export default defineMessages({

  // Site info
  world: {
    id: 'common.site-info.world',
    defaultMessage: 'World',
  },
  photoWorld: {
    id: 'common.site-info.photo-world',
    defaultMessage: 'Photo World',
  },

  // Auth
  signIn: {
    id: 'common.auth.sing-in',
    defaultMessage: 'Sign in',
  },
  signUp: {
    id: 'common.auth.sign-up',
    defaultMessage: 'Sign up',
  },
  signOut: {
    id: 'common.auth.sign-out',
    defaultMessage: 'Sign out',
  },

  // Actions
  prevStep: {
    id: 'common.actions.prev-step',
    defaultMessage: 'Prev step',
  },
  nextStep: {
    id: 'common.actions.next-step',
    defaultMessage: 'Next step',
  },
  skipStep: {
    id: 'common.actions.skip-step',
    defaultMessage: 'Skip step',
  },
  finish: {
    id: 'common.actions.finish',
    defaultMessage: 'Finish',
  },
  or: {
    id: 'common.actions.or',
    defaultMessage: 'OR',
  },
  send: {
    id: 'common.actions.send',
    defaultMessage: 'Send',
  },
  cancel: {
    id: 'common.actions.cancel',
    defaultMessage: 'Cancel',
  },
  choosePhoto: {
    id: 'common.actions.choose.photo',
    defaultMessage: 'Choose photo',
  },
  upload: {
    id: 'common.actions.upload',
    defaultMessage: 'Upload',
  },
  reset: {
    id: 'common.actions.reset',
    defaultMessage: 'Reset',
  },

  // User fields
  email: {
    id: 'common.user-fields.email',
    defaultMessage: 'Email',
  },
  password: {
    id: 'common.user-fields.password',
    defaultMessage: 'Password',
  },
  retryPassword: {
    id: 'common.user-fields.retry-password',
    defaultMessage: 'Retry password',
  },


  firstName: {
    id: 'common.user-fields.first-name',
    defaultMessage: 'First name',
  },
  lastName: {
    id: 'common.user-fields.last-name',
    defaultMessage: 'Last name',
  },

  birthDay: {
    id: 'common.user-fields.birthday',
    defaultMessage: 'Birthday',
  },

  aboutMe: {
    id: 'common.user-fields.about-me',
    defaultMessage: 'About me',
  },

  phone: {
    id: 'common.user-fields.phone',
    defaultMessage: 'Phone number',
  },
  website: {
    id: 'common.user-fields.website',
    defaultMessage: 'Web site',
  },
  city: {
    id: 'common.user-fields.city',
    defaultMessage: 'City',
  },

});
