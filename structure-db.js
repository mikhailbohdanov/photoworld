
const GENDER = {
  MALE: 'FEMALE',
  FEMALE: 'FEMALE',
};

const MESSAGE_TYPE = {
  IN: 'IN',
  OUT: 'OUT',
};


export default {
  userProfiles: {
    ['$userId']: {
      firstName: String,
      lastName: String,
      photo: String,
    },
  },
  userProfilesDetails: {
    ['$userId']: {
      birthDay: Date,
      gender: GENDER,
      backgroundPhoto: String,
    },
  },

  userConversations: {
    ['$userId']: {
      ['$conversationId']: {
        userId: 'userId',
        lastText: String,
        lastTime: Number,
      },
    },
  },
  conversationMessages: {
    ['$conversationId']: {
      ['$messageId']: {
        time: Number,
        text: String,
        type: MESSAGE_TYPE,
      },
    },
  },

  userGalleries: {
    ['$userId']: {
      ['$galleryId']: {
        name: String,
        description: String,
      },
    },
  },
  galleryPhotos: {
    ['$galleryId']: {
      ['$photoId']: {
        url: String,
        meta: {},
      },
    },
  },

};
