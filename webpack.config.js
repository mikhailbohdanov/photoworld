switch (process.env.NODE_ENV) {
  case 'prod':
  case 'production':
    module.exports = require('./webpack.config/production')('production');
    break;
  case 'dev':
  case 'develop':
  default:
    module.exports = require('./webpack.config/develop')('develop');
}
