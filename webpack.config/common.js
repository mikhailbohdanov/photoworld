const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

const helpers = require('./helpers');

const NoEmitOnErrorsPlugin = webpack.NoEmitOnErrorsPlugin;
const ProvidePlugin = webpack.ProvidePlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const JSX_LOADER = {
  test: /\.jsx?$/,
  include: [helpers.src()],
  loader: 'babel-loader',
};
const CSS_LOADER = {
  test: /\.css$/,
  include: [helpers.src(), helpers.root('node_modules')],
  loader: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: false,
          importLoaders: 1,
        }
      },
    ],
  }),
};
const STYLUS_LOADER = {
  test: /\.styl$/,
  include: [helpers.src()],
  loader: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: true,
          importLoaders: 1,
          localIdentName: '[path]-[name]__[local]___[hash:base64:5]',
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: [
            autoprefixer({
              browsers: ['> 5%'],
            }),
          ],
        },
      },
      'stylus-loader',
    ]
  }),
};

const PLUGINS = [
  new NoEmitOnErrorsPlugin(),
  new ExtractTextPlugin({
    filename: 'style.css',
    allChunks: true,
    disable: true,
  }),
  new HtmlWebpackPlugin({
    template: helpers.src('index.html'),
    files: { css: ['style.css'], js: [ 'main.js'] }
  }),
  new ProvidePlugin({ '_': ['lodash'] }),
  new CopyWebpackPlugin([{ from: helpers.src('views', 'static'), to: 'static' }]),
];


module.exports = function () {
  return {
    context: helpers.src(),
    entry: { main: './main' },
    resolve: {
      extensions: ['.js'],
      modules: [helpers.src(), helpers.root('node_modules')],
    },
    resolveLoader: { extensions: ['.js'] },
    module: {
      rules: [
        JSX_LOADER,
        CSS_LOADER,
        STYLUS_LOADER,
        {
          include: helpers.src(),
          test: /\.json$/,
          loader: 'json-loader',
        },
      ],
    },
    plugins: PLUGINS,
  };
};
